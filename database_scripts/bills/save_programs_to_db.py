import pymysql
import json
import requests

host = "aa129i85zy4vgvy.crgt7cpj8d6t.us-east-2.rds.amazonaws.com"
dbname = "EADB"
user = "Mercury"
password = "earthfirst"
charset = "utf8mb4"
cursorclass = pymysql.cursors.DictCursor

connection = pymysql.connect(
    db=dbname,
    host=host,
    user=user,
    password=password,
    charset=charset,
    cursorclass=cursorclass,
)


def populate_db():
    # Get propublica data with API call
    unique_bills = set()

    HEADERS = {"X-API-Key": "70GG0O6kaErhc1x5yYD93k0M4iWd7k6IfzHffVUp"}
    URL = "https://api.propublica.org/congress/v1/bills/search.json"
    PARAMS = {"query": "environment"}

    payload = requests.get(url=URL, params=PARAMS, headers=HEADERS)
    payload = json.loads(payload.text)
    data = payload["results"][0]["bills"]

    write_to_db(data, unique_bills)

    assert len(unique_bills) > 0

    PARAMS = {"query": "recycling"}
    payload = requests.get(url=URL, params=PARAMS, headers=HEADERS)
    payload = json.loads(payload.text)
    data = payload["results"][0]["bills"]

    write_to_db(data, unique_bills)

    connection.close()


def write_to_db(data, unique_bills):
    for item in data:
        bill_id = item["bill_id"]
        if bill_id not in unique_bills:
            unique_bills.add(bill_id)

            description = item["title"]
            title = item["short_title"]
            sponsor_name = item["sponsor_name"]
            sponsor_state = item["sponsor_state"]
            sponsor_party = item["sponsor_party"]
            bill_pdf = item["gpo_pdf_uri"]
            intro_date = item["introduced_date"]
            latest_major_action = item["latest_major_action"]
            sponsor_image = "None"

            wiki_title = sponsor_name.split()
            wiki_title = wiki_title[0] + "_" + wiki_title[-1]

            url = (
                "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles="
                + wiki_title
            )
            payload = requests.get(url=url)
            payload = json.loads(payload.text)
            payload = payload["query"]["pages"]
            print(wiki_title)
            id = list(payload.keys())[0]
            if id != -1:
                try:
                    sponsor_image = payload[id]["original"]["source"]
                except:
                    pass

            try:
                with connection.cursor() as cursor:
                    sql = "INSERT INTO `bills` (`title`, `description`, `sponsor_name`, `sponsor_state`, `sponsor_party`, `bill_pdf`, `intro_date`, `latest_major_action`, `sponsor_image`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                    cursor.execute(
                        sql,
                        (
                            title,
                            description,
                            sponsor_name,
                            sponsor_state,
                            sponsor_party,
                            bill_pdf,
                            intro_date,
                            latest_major_action,
                            sponsor_image,
                        ),
                    )
                    connection.commit()
            finally:
                pass


def main():
    populate_db()


if __name__ == "__main__":
    main()

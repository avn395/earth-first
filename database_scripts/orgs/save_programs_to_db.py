import pymysql
import json
import requests
#import get_org_images

host = "aa129i85zy4vgvy.crgt7cpj8d6t.us-east-2.rds.amazonaws.com"
dbname = "EADB"
user = "Mercury"
password = "earthfirst"
charset = "utf8mb4"
cursorclass = pymysql.cursors.DictCursor

connection = pymysql.connect(
    db=dbname,
    host=host,
    user=user,
    password=password,
    charset=charset,
    cursorclass=cursorclass,
)


def populate_db():
    URL = "https://api.data.charitynavigator.org/v2/Organizations?app_id=bc83f70e&app_key=3ac156881d534b2b5dfd998abfc073b3&search=Environment&pageSize =25"

    data = requests.get(url=URL)
    data = json.loads(data.text)

    for item in data:
        name = item["charityName"]
        mission = item["mission"]
        tagline = item["tagLine"]
        url = item["websiteURL"]

        addr = item["mailingAddress"]

        state = addr["stateOrProvince"]
        city = addr["city"]
        address = addr["streetAddress1"]
        if addr["streetAddress2"] != None:
            address += " " + addr["streetAddress2"]
        postal_code = addr["postalCode"]
        try:
            rating = item["currentRating"]["rating"]
        except:
            rating = None

        try:
            with connection.cursor() as cursor:
                sql = "INSERT INTO `orgs` (`name`, `mission`, `tagline`, `url`, `state`, `city`, `address`, `postal_code`, `rating`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(
                    sql,
                    (
                        name,
                        mission,
                        tagline,
                        url,
                        state,
                        city,
                        address,
                        postal_code,
                        rating,
                    ),
                )
                connection.commit()
        finally:
            pass
    connection.close()

def add_one_images():
    names, urls = get_org_images.clean_output_file()
    image_url = "http://womenforahealthyenvironment.org/images/head-whe-logo.png"
    name = "Women for A Healthy Environment"
    try:
        with connection.cursor() as cursor:
            cursor.execute("UPDATE organizations SET image_url=%s WHERE name=%s",(image_url, name))
            connection.commit()
    except:
        pass
    connection.close()

def add_org_images():
    names, urls = get_org_images.clean_output_file()
    for i in range(len(names)):
        image_url = urls[i].strip()
        name = names[i].strip()
        try:
            with connection.cursor() as cursor:
                cursor.execute("UPDATE organizations SET image_url=%s WHERE name=%s",(image_url, name))
                connection.commit()
        except:
            pass
    connection.close()

def add_org_incomes():
    URL = "https://api.data.charitynavigator.org/v2/Organizations?app_id=bc83f70e&app_key=3ac156881d534b2b5dfd998abfc073b3&search=Environment&pageSize =25"

    data = requests.get(url=URL)
    data = json.loads(data.text)

    for item in data:
        name = item["charityName"]
        incomeAmount = item["irsClassification"]["incomeAmount"]
        try:
            with connection.cursor() as cursor:
                cursor.execute("UPDATE organizations SET incomeAmount=%s WHERE name=%s",(incomeAmount, name))
                connection.commit()
        except:
            pass
    connection.close()

def main():
    add_org_incomes()


if __name__ == "__main__":
    main()

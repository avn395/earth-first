from google_images_download import google_images_download

def get_images():
    response = google_images_download.googleimagesdownload()

    URL = "https://earth-first.live/api/orgs?per_page=1000"
    payload = requests.get(url=URL)
    payload = json.loads(payload.text)

    for item in payload:
        name = item["name"]
        
        args = { "keywords" : name, "prefix" : name, "limit" : 1, "safe_search" : True, "no_download": True, "no_directory" : True}

        response.download(args)

def clean_output_file():
    names = []
    urls = []
    with open("output.txt", "r") as f:
        lines = f.readlines()
        for line in lines:
            if "Item no.:" in line:
                names.append(line[28:])
            elif "Image URL:" in line:
                urls.append(line[11:])
    
    return names, urls

def main():
    pass

if __name__ == '__main__':
    main()
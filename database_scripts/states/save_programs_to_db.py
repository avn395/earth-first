import pymysql
import json
import requests

host = "aa129i85zy4vgvy.crgt7cpj8d6t.us-east-2.rds.amazonaws.com"
dbname = "EADB"
user = "Mercury"
password = "earthfirst"
charset = "utf8mb4"
cursorclass = pymysql.cursors.DictCursor

connection = pymysql.connect(
    db=dbname,
    host=host,
    user=user,
    password=password,
    charset=charset,
    cursorclass=cursorclass,
)


def get_cities_dict():
    with open("cities_data.json") as f:
        data = json.load(f)

    cities = {}

    for d in data:
        state = d["Statefederal districtor territory"]
        city = []
        if d["Most populous"] != "":
            city.append(d["Most populous"])
        if d["2nd most populous"] != "":
            city.append(d["2nd most populous"])
        if d["3rd most populous"] != "":
            city.append(d["3rd most populous"])
        if d["4th most populous"] != "":
            city.append(d["4th most populous"])
        if d["5th most populous"] != "":
            city.append(d["5th most populous"])

        cities[state] = city

    return cities


def get_program_ids(data):
    # data is the return of get_cities_dict
    with open("state_abbreviations.json") as f:
        state_abbr = json.load(f)

    programids = set()

    for state in data:
        abbr = state_abbr[state]
        cities = data[state]
        for city in cities:
            # earth911.searchPostalDataPhonetic for long, lat
            PARAMS = {
                "api_key": "a9ad3314bb3ac243",
                "country": "US",
                "province": abbr,
                "query": city,
            }
            URL = "http://api.earth911.com/earth911.searchPostalDataPhonetic"
            payload = requests.get(url=URL, params=PARAMS)
            payload = json.loads(payload.text)

            results = payload["result"]
            if len(results) > 0:
                longitude = payload["result"][0]["longitude"]
                latitude = payload["result"][0]["latitude"]

                # earth911.searchPrograms find program ids by long,lat
                PARAMS = {
                    "api_key": "a9ad3314bb3ac243",
                    "longitude": longitude,
                    "latitude": latitude,
                    "max_results": 10,
                }
                URL = "https://api.earth911.com/earth911.searchPrograms"
                payload = requests.get(url=URL, params=PARAMS)
                payload = json.loads(payload.text)
                results = payload["result"]

                # store set of program ids
                for result in results:
                    programids.add(result["program_id"])
    # return set of program ids
    a = list(programids)
    with open("programids.json", "w") as output:
        json.dump(a, output)
    return programids


def populate_db():
    with open("programids.json") as f:
        data = json.load(f)
    data = data[0:50]
    with open("program_types.json") as f:
        program_types = json.load(f)
    entries = []
    for item in data:
        PARAMS = {"api_key": "a9ad3314bb3ac243", "program_id": item}
        URL = "https://api.earth911.com/earth911.getProgramDetails"
        payload = requests.get(url=URL, params=PARAMS)
        payload = json.loads(payload.text)

        obj = {}

        result = payload["result"][item]
        obj["Id"] = item
        obj["Name"] = result["description"]
        obj["City"] = result["city"]
        obj["State"] = result["province"]
        obj["Hours"] = result["hours"]
        obj["Phone"] = result["phone"]
        obj["Description"] = result["notes"]
        obj["Url"] = result["url"]
        obj["ProgramType"] = program_types[result["program_type_id"]]

        entries.append(obj)

        result = payload["result"][item]
        name = result["description"]
        city = result["city"]
        state = result["province"]
        hours = result["hours"]
        phone = result["phone"]
        desc = result["notes"]
        url = result["url"]
        program_type = program_types[result["program_type_id"]]

        try:
            with connection.cursor() as cursor:
                sql = "INSERT INTO `programs` (`Id`, `Name`, `City`, `State`, `Hours`, `Phone`, `Description`, `Url`, `ProgramType`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(
                    sql,
                    (item, name, city, state, hours, phone, desc, url, program_type),
                )
            connection.commit()
        except:
            pass

    with open("db_entries.json", "w") as f:
        json.dump(entries, f)
    connection.close()

    # data is return of get_program_ids
    # use earth911.getProgramDetails on each program id
    # connect to mysql db
    # for each program:
    # take {program id as  (item), description as ProgramName, city as city, province as state,
    # hours as hours, phone as phone, notes as description, url as url, translated program type as programtype}


def get_state_flag(abbr):
    # https://res.cloudinary.com/fen-learning/image/upload/c_limit,w_250,h_125/infopls_images/images/ak_fl.gif
    a = abbr.lower()
    image_url = (
        "https://res.cloudinary.com/fen-learning/image/upload/c_limit,w_250,h_125/infopls_images/images/"
        + a
        + "_fl.gif"
    )
    return image_url


def make_states_table():
    with open("state_abbreviations.json") as f:
        state_abbr = json.load(f)

    for state, abbr in state_abbr.items():
        try:
            with connection.cursor() as cursor:
                sql = "INSERT INTO `states` (`abbr`, `state`) VALUES (%s, %s)"
                cursor.execute(sql, (abbr, state))
            connection.commit()
        except:
            pass
    connection.close()


def add_state_images():
    with open("state_abbreviations.json") as f:
        state_abbr = json.load(f)
    for state, abbr in state_abbr.items():
        image_url = get_state_flag(abbr)
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    """
                   UPDATE states
                   SET image_url=%s
                   WHERE abbr=%s
                """,
                    (image_url, abbr),
                )
            connection.commit()
        except:
            pass
    connection.close()

    def add_state_region():
        regions = {"Far West": ["California", "Hawaii", "Nevada"],
                   "Great Lakes": ["Illinois", "Indiana", "Michigan", "Minnesota", "Ohio", "Wisconsin"],
                   "Mountain West": ["Arizona", "Colorado", "Idaho", "Montana","New Mexico", "Utah", "Wyoming"],
                   "Midsouth": ["Delaware", "District of Columbia", "Kentucky", "Maryland", "North Carolina", "Tennessee", "Virginia", "West Virginia"],
                   "Midwest": ["Iowa", "Kansas", "Missouri", "Nebraska", "North Dakota", "South Dakota"],
                   "New England": ["Connecticut", "Maine", "Massachusetts", "New Hampshire", "Rhode Island", "Vermont"],
                   "Northeast": ["New Jersey", "New York", "Pennsylvania"],
                   "Northwest": ["Oregon", "Washington", "Alaska"],
                   "South Central": ["Arkansas", "Louisiana", "Oklahoma", "Texas"],
                   "Southeast": ["Alabama", "Florida", "Georgia", "Mississippi", "South Carolina"]
        }
        for region, state_list in regions.items():
            for s in state_list:
                try:
                    with connection.cursor() as cursor:
                        cursor.execute(
                        """
                        UPDATE states
                        SET region=%s
                        WHERE state=%s
                        """,
                            (region, s),
                        )
                    connection.commit()
                except:
                    pass
        connection.close()


def main():
    # cities = get_cities_dict()
    # ids = get_program_ids(cities)
    # populate_db()
    # make_states_table()
    add_state_images()


if __name__ == "__main__":
    main()

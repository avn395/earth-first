from application import db
import json


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)

    def __repr__(self):
        return "<User %r>" % self.username


class About(db.Model):
    __tablename__ = "about"
    name = db.Column(db.Unicode(64), primary_key=True)
    commit_count = db.Column(db.Integer())
    last_commit = db.Column(db.Text())
    issues_opened = db.Column(db.Integer())
    issues_closed = db.Column(db.Integer())
    last_issue = db.Column(db.Text())

    def __repr__(self):
        return str(self.name)


# class States(db.Model):
#    __tablename__ = "states"
#    state_name = db.Column(db.Text(), primary_key = True)
#    senators = db.Column(db.Text())


class Program(db.Model):
    __tablename__ = "programs"
    Id = db.Column(db.Text(), primary_key=True)
    Name = db.Column(db.Text())
    City = db.Column(db.Text())
    State = db.Column(db.Text())
    Hours = db.Column(db.Text())
    Phone = db.Column(db.Text())
    Description = db.Column(db.Text())
    Url = db.Column(db.Text())
    ProgramType = db.Column(db.Text())

    @property
    def serialize(self):
        return {
            "Id": self.Id,
            "Name": self.Name,
            "City": self.City,
            "State": self.State,
            "Hours": self.Hours,
            "Phone": self.Phone,
            "Description": self.Description,
            "Url": self.Url,
            "ProgramType": self.ProgramType,
        }


class Org(db.Model):
    __tablename__ = "organizations"
    name = db.Column(db.Text(), primary_key=True)
    mission = db.Column(db.Text())
    tagline = db.Column(db.Text())
    url = db.Column(db.Text())
    state = db.Column(db.Text())
    city = db.Column(db.Text())
    address = db.Column(db.Text())
    postal_code = db.Column(db.Text())
    rating = db.Column(db.Text())
    image_url = db.Column(db.Text())
    incomeAmount = db.Column(db.Integer())

    @property
    def serialize(self):
        return {
            "name": self.name,
            "mission": self.mission,
            "tagline": self.tagline,
            "url": self.url,
            "state": self.state,
            "city": self.city,
            "address": self.address,
            "postal_code": self.postal_code,
            "rating": self.rating,
            "image_url": self.image_url,
            "incomeAmount": self.incomeAmount,
        }


class Bill(db.Model):
    __tablename__ = "Bills"
    bill_id = db.Column(db.Text(), primary_key=True)
    title = db.Column(db.Text())
    description = db.Column(db.Text())
    sponsor_name = db.Column(db.Text())
    sponsor_state = db.Column(db.Text())
    sponsor_party = db.Column(db.Text())
    sponsor_image = db.Column(db.Text())
    bill_pdf = db.Column(db.Text())
    intro_date = db.Column(db.Date())
    latest_major_action = db.Column(db.Text())

    @property
    def serialize(self):
        return {
            "bill_id": self.bill_id,
            "title": self.title,
            "description": self.description,
            "sponsor_name": self.sponsor_name,
            "sponsor_state": self.sponsor_state,
            "sponsor_party": self.sponsor_party,
            "sponsor_image": self.sponsor_image,
            "bill_pdf": self.bill_pdf,
            "intro_date": self.intro_date,
            "latest_major_action": self.latest_major_action,
        }


class State(db.Model):
    __tablename__ = "states"
    abbr = db.Column(db.Text(), primary_key=True)
    state = db.Column(db.Text())
    population = db.Column(db.Integer())
    image_url = db.Column(db.Text())
    num_bills = db.Column(db.Integer())
    num_orgs = db.Column(db.Integer())
    num_progs = db.Column(db.Integer())
    region = db.Column(db.Text())

    @property
    def serialize(self):
        return {
            "abbr": self.abbr,
            "state": self.state,
            "population": self.population,
            "image_url": self.image_url,
            "num_bills": self.num_bills,
            "num_orgs": self.num_orgs,
            "num_progs": self.num_progs,
            "region": self.region,
        }

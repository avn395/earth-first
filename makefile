server : dev
	python3 server.py

server_mac : dev_mac
	python3 server.py

test :
	python3 test/tests.py

sel:
	python3 test/guitests.py

post:
	node_modules/.bin/newman run test/Postman.json

dev:
	find templates/ -type f -exec sed -i 's/https:\/\/earth-first.live/http:\/\/localhost:5000/g' {} +
	find static/js -type f -exec sed -i 's/https:\/\/earth-first.live/http:\/\/localhost:5000/g' {} +

prod:
	find templates/ -type f -exec sed -i 's/http:\/\/localhost:5000/https:\/\/earth-first.live/g' {} +
	find static/js -type f -exec sed -i 's/http:\/\/localhost:5000/https:\/\/earth-first.live/g' {} +

dev_mac:
	find templates/ -type f -exec sed -i '' 's/https:\/\/earth-first.live/http:\/\/localhost:5000/g' {} +
	find static/js -type f -exec sed -i '' 's/https:\/\/earth-first.live/http:\/\/localhost:5000/g' {} +

prod_mac:
	find templates/ -type f -exec sed -i '' 's/http:\/\/localhost:5000/https:\/\/earth-first.live/g' {} +
	find static/js -type f -exec sed -i '' 's/http:\/\/localhost:5000/https:\/\/earth-first.live/g' {} +

push_mac: prod_mac
	git push

push: prod
	git push

pull_mac: prod_mac
	git pull

pull: prod
	git pull

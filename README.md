# Earth First

### Group members

| Name              | Eid     | Gitlab Id    |
|-------------------|---------|--------------|
| Rizwan Lubis      | ral3232 | ralubis      |
| Avni Nandu        | avn395  | avnandu      |
| Rileigh Bandy     | rb36955 | rileighbandy |
| Michael Lee       | ml45898 | michaellee4  |
| Yaswin Veluguleti | yv2354  | yaswin       |

Git SHA:
80d4d9cf734a3f46a94b6d4c2f59dfaffbf4c9af

GitLab pipelines link:
https://gitlab.com/avn395/earth-first/pipelines

Link to website:
https://earth-first.live

Estimated completion time for each member: 10hr/each.
Actual completion time for each member:
| Name              | Time (hr)|  
|-------------------|----------|
| Rizwan Lubis      | 14       |
| Avni Nandu        |  5       |
| Rileigh Bandy     |  8       |
| Michael Lee       |  5       |
| Yaswin Veluguleti |  5       |

Comments:
Currently, our database is missing some instances, and we have no defaults. Attributes like tagline may appear missing because they are currently null. We will fill these in manually later.
Also, the YouTube API has a limit, and if it runs out random videos show up. The limit resets everyday.
What should be main.py is actually called application.py. For index.js, please refer to index.html.
Visualizations can be found in the Visualizations and Customer Visualizations tabs on the navigation bar.

### Resources Used
* http://bl.ocks.org/NPashaP/a74faf20b492ad377312
* https://www.w3schools.com/css/css_howto.asp
* https://getbootstrap.com/docs/3.4/css/#buttons
* https://stackoverflow.com/questions/22259847/application-not-picking-up-css-file-flask-python
* https://getbootstrap.com/docs/4.1/components/navbar/
* https://stackoverflow.com/questions/22383547/bootstrap-dropdown-menu-is-not-working

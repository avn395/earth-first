
class BillInstance extends React.Component {
  render() {
    return (
      <div className="container">
        <BillSummary info={this.props.bill_props.bill_summary}/>
        <BillDetails info={this.props.bill_props.bill_details}/>
        <BillOrgEntries info={this.props.bill_props.bill_orgs}/>
      </div>
    );
  }
}


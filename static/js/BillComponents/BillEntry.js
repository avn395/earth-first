class BillEntry extends React.Component {
  render() {
    return (
      <div
        className="card h-100 card-body"
        >
      <img
            src={this.props.sponsor_image}
            className="card-img-top"
          />
        
        <div className="card-body">
          <a href={this.props.url}>
          {this.props.title}
          </a>
          <br/>
          Sponsored by: {this.props.sponsor_name} <br/>
          Political Party: {this.props.sponsor_party} <br/>
          State of Sponsor: {this.props.sponsor_state} <br/>
        </div>
      </div>
    );
  }
}



class BillDetails extends React.Component {
  render() {
    return (
      <div className="container">
        <h2>Bill Details</h2>
        <simple_grid>
          <div className="row">
            <div
              className="col-12"
              style={{
                backgroundColor: "silver"
              }}
            >
              This bill was introduced on {this.props.info.intro_date}
            </div>
            <div
              className="col-12"
              style={{
                backgroundColor: "grey"
              }}
            >
              Latest major action:{" "} {this.props.info.action}
            </div>
            <div
              className="col-12"
              style={{
                backgroundColor: "silver"
              }}
            >
              Link to a pdf of the bill: <a href={this.props.info.pdf_url}>here</a>
            </div>
          </div>
        </simple_grid>
      </div>
    );
  }
}

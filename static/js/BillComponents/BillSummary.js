
class BillSummary extends React.Component {
  render() {
    return (
      <div className="container">
        <BillTitle info={this.props.info}/>
        <BillMedia info={this.props.info}/>
        <hr />
      </div>
    );
  }
}


// import OrgEntry from "./OrgEntry";
class BillEntries extends React.Component {
  constructor(props) {
    super(props);
    this.orig_items = this.props.entries.map((d, i)=> <BillEntry url={d.url} title={d.title} sponsor_image={d.sponsor_image} sponsor_name={d.sponsor_name} sponsor_state={d.sponsor_state} sponsor_party={d.sponsor_party} intro_date={d.intro_date} latest_major_action={d.latest_major_action} key={i}/>);

    this.toggles = {
      sortByNameAlphabet: false,
    };

    this.state = {
      state_filter_name: "Filter by State",
      party_filter_name: "Filter by Party",
      sponsor_filter_name: "Filter by Sponsor",
      sort_name: "Sort",
      sorted_items: this.orig_items,
      list_items: this.orig_items.slice(0,9),
      current_page : 1,
      searchValue: '',
    };

    this.filterByState = this.filterByState.bind(this);
    this.filterByParty = this.filterByParty.bind(this);
    this.filterBySponsor = this.filterBySponsor.bind(this);
    this.generateFilterStateDropdown = this.generateFilterStateDropdown.bind(this);
    this.generateFilterPartyDropdown = this.generateFilterPartyDropdown.bind(this);
    this.generateFilterSponsorDropdown = this.generateFilterSponsorDropdown.bind(this);
    this.generatePagination = this.generatePagination.bind(this);

    this.sortBySponsorAlphabetAZ = this.sortBySponsorAlphabetAZ.bind(this);
    this.sortByNameAlphabetAZ = this.sortByNameAlphabetAZ.bind(this);
    this.sortBySponsorAlphabetZA = this.sortBySponsorAlphabetZA.bind(this);
    this.sortByNameAlphabetZA = this.sortByNameAlphabetZA.bind(this);

    this.reset = this.reset.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
  }

  filterByState(sponsor_state) {
    this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.sponsor_state.includes(sponsor_state));
    this.selectPage(1);
    this.setState({
      sort_name: "Sort",
      state_filter_name: "Filter by State: " + sponsor_state,
      current_page : 1
    });
  }

  filterByParty(sponsor_party) {
       this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.sponsor_party.includes(sponsor_party));
       this.selectPage(1);
       this.setState({
        sort_name: "Sort",
        party_filter_name: "Filter by Party: " + sponsor_party,
        current_page : 1
      });
 }

 filterBySponsor(sponsor_name) {
      this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.sponsor_name.includes(sponsor_name));
      this.selectPage(1);
      this.setState({
       sort_name: "Sort",
       sponsor_filter_name: "Filter by Sponsor: " + sponsor_name,
       current_page : 1
     });
}

  generateFilterStateDropdown() {
    var entries = new Set();
    for (var i in this.props.entries){
      entries.add(this.props.entries[i].sponsor_state);
    }
    return [...entries].sort().map(entry => {
      return(
        <div class="dropdown-item" onClick={()=>{this.filterByState(entry)}}>{entry}</div>
      );
        });
  }

  generateFilterPartyDropdown() {
      var entries = new Set();
      for (var i in this.props.entries){
        entries.add(this.props.entries[i].sponsor_party);
      }
      return [...entries].sort().map(entry => {
        return(
          <div class="dropdown-item" onClick={()=>{this.filterByParty(entry)}}>{entry}</div>
        );
          });
 }

 generateFilterSponsorDropdown() {
     var entries = new Set();
     for (var i in this.props.entries){
       entries.add(this.props.entries[i].sponsor_name);
     }
     return [...entries].sort().map(entry => {
       return(
         <div class="dropdown-item" onClick={()=>{this.filterBySponsor(entry)}}>{entry}</div>
       );
         });
}

  generatePagination() {
    var arr = Array.from({length: Math.ceil(this.state.sorted_items.length/9)}, (v, k) => k+1);
    return [...arr].map(entry => {
      return(
        <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(entry)}}>{entry}</a>
          </li>
      )
    })
  }

  sortBySponsorAlphabetAZ(){
    var myData = [].concat(this.orig_items);
    myData.sort(function(a, b) {
      return a.props.sponsor_name < b.props.sponsor_name ? -1 : a.props.sponsor_name > b.props.sponsor_name ? 1 : 0;});

    this.setState({
      sorted_items: myData,
      sort_name: "Sort by: Sponsor Name Alphabetically A-Z",
      state_filter_name: "Filter by State",
      party_filter_name: "Filter by Party",
      sponsor_filter_name: "Filter by Sponsor",
      list_items : myData.slice(0,9),
      current_page : 1

    });
  }

  sortBySponsorAlphabetZA(){
    var myData = [].concat(this.orig_items);
    myData.sort(function(a, b) {
      return a.props.sponsor_name < b.props.sponsor_name ? 1 : a.props.sponsor_name > b.props.sponsor_name ? -1 : 0;});

    this.setState({
      sorted_items: myData,
      sort_name: "Sort by: Sponsor Name Alphabetically Z-A",
      state_filter_name: "Filter by State",
      party_filter_name: "Filter by Party",
      sponsor_filter_name: "Filter by Sponsor",
      list_items : myData.slice(0,9),
      current_page : 1

    });
  }

  sortByNameAlphabetAZ(){
    var myData = [].concat(this.orig_items);
    myData.sort(function(a, b) {
      return a.props.title < b.props.title ? -1 : a.props.title > b.props.title ? 1 : 0;});

    this.setState({
      sorted_items: myData,
      sort_name: "Sort by: State Name Alphabetically",
      state_filter_name: "Filter by State",
      party_filter_name: "Filter by Party",
      sponsor_filter_name: "Filter by Sponsor",
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  sortByNameAlphabetZA(){
    var myData = [].concat(this.orig_items);
    myData.sort(function(a, b) {
      return a.props.title < b.props.title ? 1 : a.props.title > b.props.title ? -1 : 0;});

    this.setState({
      sorted_items: myData,
      sort_name: "Sort by: State Name Reverse Alphabetically",
      state_filter_name: "Filter by State",
      party_filter_name: "Filter by Party",
      sponsor_filter_name: "Filter by Sponsor",
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  reset() {
    this.setState({
      state_filter_name: "Filter by State",
      party_filter_name: "Filter by Party",
      sponsor_filter_name: "Filter by Sponsor",
      sort_name: "Sort",
      sorted_items: this.orig_items,
      list_items: this.orig_items.slice(0,9),
      current_page : 1,
      searchValue: ''
    })
  }

  handleNext() {
    if(this.state.current_page < Math.ceil(this.state.sorted_items.length/9)){
      this.selectPage(this.state.current_page + 1);
    }
  }

  handlePrevious() {
    if(this.state.current_page > 1){
      this.selectPage(this.state.current_page - 1);
    }
  }

  selectPage(page) {
    var end = page * 9;
    var start = end - 9;
    this.setState({
      current_page : page,
      list_items: this.state.sorted_items.slice(start,end)
    })
  }

  updateSearchValue(event) {
    this.setState({
      searchValue: event.target.value
    });
  }

  render() {
    return (
      <div className="container">
        <div class="form-row align-items-center">
          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.sort_name}
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <div class="dropdown-item" onClick={this.sortBySponsorAlphabetAZ}>By Sponsor Name Alphabetically A-Z</div>
                <div class="dropdown-item" onClick={this.sortBySponsorAlphabetZA}>By Sponsor Name Alphabetically Z-A</div>
                <div class="dropdown-item" onClick={this.sortByNameAlphabetAZ}>By Bill Name Alphabetically A-Z</div>
                <div class="dropdown-item" onClick={this.sortByNameAlphabetZA}>By Bill Name Alphabetically Z-A</div>
              </div>
            </div>
          </div>

          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.state_filter_name}
              </button>
              <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
                {this.generateFilterStateDropdown()}
              </div>
            </div>
          </div>

          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.party_filter_name}
              </button>
              <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
                {this.generateFilterPartyDropdown()}
              </div>
            </div>
          </div>

          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.sponsor_filter_name}
              </button>
              <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
                {this.generateFilterSponsorDropdown()}
              </div>
            </div>
          </div>

          <div class="col-auto">
                <input type="search" class="form-control" id="search" placeholder="Enter Search Term"
                onChange={event => this.updateSearchValue(event)}
                onKeyPress={event => {if (event.key === "Enter") {location.replace("https://earth-first.live/politics_search/" + this.state.searchValue)} }} />
          </div>


          <div class="col-auto">
            <button type="button" class="btn btn-danger" onClick={this.reset}>Reset</button>
          </div>
        </div>

        <br />

        <div className="card-columns">
           {this.state.list_items}
       </div>

       <ul class="pagination pg-blue justify-content-center">
          <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(1)}} >First</a>
          </li>
          <li class="page-item">
            <a class="page-link" onClick={this.handlePrevious}>Previous</a>
          </li>
          {this.generatePagination()}
          <li class="page-item">
            <a class="page-link" onClick={this.handleNext}>Next</a>
          </li>
          <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(5)}} >Last</a>
          </li>
        </ul>
      </div>
    );
  }
}


class BillMedia extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="media">
          <div className="media-left">
            <img
              src={this.props.info.img_src}
              style={{
                maxWidth: "300px",
                maxHeight: "300px"
              }}
            />
          </div>
          <div className="media-body">
            <h4 className="media-heading">
              Bill Sponsor: {this.props.info.sponsor} 
            </h4>
            <p>
              state:{" "}
              <a href={'/states/'+this.props.info.state}>
                {this.props.info.state}
              </a>
            </p>
            <p>
              political party: {" "} {this.props.info.party}
            </p>
          </div>
        </div>
        <br />
        <br />
        <center>
          <iframe width="420" height="315"
            src={"https://www.youtube.com/embed/" + this.props.info.youtube_sponsor_id} allow="autoplay; encrypted-media" allowFullScreen>
          </iframe> 
          <iframe width="420" height="315"
            src={"https://www.youtube.com/embed/" + this.props.info.youtube_bill_id} allow="autoplay; encrypted-media" allowFullScreen>
          </iframe>
        </center>
      </div>
    );
  }
}

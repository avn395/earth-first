
class BillTitle extends React.Component {
  render() {
    return (
      <div>
        <h1>
        {this.props.info.title}   
        </h1>
        <p />
        <p>
          <h3>
          {this.props.info.text}
          </h3>
        </p>
      </div>
    );
  }
}


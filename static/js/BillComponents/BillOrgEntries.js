class BillOrgEntries extends React.Component {
    render() {
      const list_items = this.props.info.map((d, i)=><BillOrgEntry url={d.url} name={d.name} key={i}/>);
      return (
        <div>
          <br/>
          <br/>
          <h2> Organizations </h2>
          { (list_items.length > 0) && (<div>Some organizations that influenced this bill are:</div>) }
        {(list_items.length > 0) && (<grid>
          <div className="row">
            {list_items}
          </div>
         </grid>)}
        {(list_items.length == 0) && <div>There are no Environmental Organizations influencing this bill.</div>}
        <br />
        <br />
           
        </div>
      );
    }
  }

  class BillOrgEntry extends React.Component {
    render() {
      return (
        <div
        className="col-xl"
        style={{
          backgroundColor: "silver"
        }}
        >
        <a href={this.props.url}>
        {this.props.name.substring(0,50)}
        </a>
      </div>
      );
    }
  }


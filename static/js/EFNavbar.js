class Navbar extends React.Component {
    constructor(props){
        super(props);
        this.searchValue = '';
        this.highlighted = window.location.href;
    }

    updateSearchValue(event) {
        this.searchValue = event.target.value;
    }
    render() {
        return (
        <nav className="navbar navbar-expand-lg navbar-light">
              <a href="/" className="navbar-left"><img src="../static/images/earth.png" style={{width:24,height:24}} /></a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                <a href="/orgs" class={(this.highlighted.includes('/orgs') ? 'active-page' : '')}>Organizations</a>
                <a href="/politics" class={(this.highlighted.includes('/politics') ? 'active-page' : '')}>Politics</a>
                <a href="/states" class={(this.highlighted.includes('/states') ? 'active-page' : '')}> States</a>
                <a href="/about" class={(this.highlighted.includes('/about') ? 'active-page' : '')}>About Us</a>
                <a href="/visualization" class={(this.highlighted.includes('/visualization') ? 'active-page' : '')}>Our Visualizations</a>
                <a href="/customer" class={(this.highlighted.includes('/customer') ? 'active-page' : '')}>Customer Visualizations</a>
                </ul>
                <ul className="navbar-nav ml-auto">
                <input type="search" id="search" class = "form-control" placeholder="General Search"
                onChange={event => this.updateSearchValue(event)}
                onKeyPress={event => {if (event.key === "Enter" && this.searchValue != '') {location.replace("https://earth-first.live/search/" + this.searchValue)} }} />
                </ul>
            </div>
        </nav>
        );
    }
}
ReactDOM.render(<Navbar/>, document.getElementById('EFNavbar'));

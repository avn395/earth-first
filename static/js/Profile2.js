// import React from "react";
// import StatisticsTable from "./StatisticsTable";
// import CenteredList from "./CenteredList";

class Profile2 extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="CardDeck card-deck">
          <div
            className="card"
            style={{
              width: "200px"
            }}
          >
            <div className="card-header">
              <h3
                style={{
                  margin: "auto"
                }}
              >
                Rileigh Bandy
              </h3>
            </div>
            <div className="card-body">
              {" "}
              <img
                src="../static/res/AboutMe_Rileigh.png"
                className="img-fluid"
              />
            </div>
            <div className="card-footer">
              <ul
                style={{
                  listStyleType: "square"
                }}
              >
                <li>Year: Senior, Graduates 2019</li>
                <li>
                  Bio: Senior Computer Science major who likes scientific
                  computing
                </li>
                <li id="ril_commits">Commits:</li>
                <li id="ril_open">Issues Opened:</li>
                <li id="ril_closed">Issues Closed:</li>
                <li>Unit tests: 15</li>
                <li>Major Responsibilities: Python/HTML/CSS</li>
              </ul>
            </div>
          </div>
          <div
            className="card"
            style={{
              width: "200px"
            }}
          >
            <div className="card-header">
              <h3
                style={{
                  margin: "auto"
                }}
              >
                Avni Nandu
              </h3>
            </div>
            <div className="card-body">
              {" "}
              <img
                src="../static/res/AboutMe_Avni.JPG"
                className="img-fluid"
              />{" "}
            </div>
            <div className="card-footer">
              <ul
                style={{
                  listStyleType: "square"
                }}
              >
                <li>Year: Junior, Graduates 2020</li>
                <li>Bio: Junior Computer Science Major who likes Taco Bell</li>
                <li id="avni_commits">Commits:</li>
                <li id="avni_open">Issues Opened:</li>
                <li id="avni_closed">Issues Closed:</li>
                <li>Unit tests: 15</li>
                <li>
                  Major Responsibilities: Documentation, Git God, API Management
                </li>
              </ul>
            </div>
          </div>
          <div
            className="card"
            style={{
              width: "200px"
            }}
          >
            <div className="card-header">
              <h3
                style={{
                  margin: "auto"
                }}
              >
                Izzy
              </h3>
            </div>
            <div className="card-body">
              {" "}
              <img
                src="../static/res/AboutMe_Izzy.png"
                className="img-fluid"
              />{" "}
            </div>
            <div className="card-footer">
              <ul
                style={{
                  listStyleType: "square"
                }}
              >
                <li>Year: 67 dog years old</li>
                <li>
                  Bio: My life consists of hard work and sleep. But mostly
                  sleep.
                </li>
                <li>Commits: 0</li>
                <li>Issues Opened: I'm only a closer</li>
                <li>Issues Closed: 0 (I'm not a good closer)</li>
                <li>Unit tests: 0</li>
                <li>
                  Major Responsibilities: Barking orders to my subordinates
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
ReactDOM.render(<Profile2/>, document.getElementById('Profile2row'));
// export default Profile2;

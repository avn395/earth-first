// import OrgEntry from "
class OrgEntries extends React.Component {

  constructor(props) {
    super(props);
    this.orig_items = this.props.entries.map((d, i)=> <OrgEntry url={d.url} content={d.content} logo_url={d.logo_url} tagline={d.tagline} incomeAmount={d.incomeAmount} city={d.city} state={d.state} rating={d.rating} key={i} />);

    this.toggles = {
      alphabet : false,
      incomeAmount : false,
      rating: false,
    };

    this.state = {
      sort_name: "Sort",
      state_filter_name: "Filter by State",
      rating_filter_name: "Filter by Rating",
      income_filter_name: "Filter by Income",
      sorted_items: this.orig_items,
      list_items: this.orig_items.slice(0,9),
      current_page : 1,
      searchValue: '',
    };

    this.filterByState = this.filterByState.bind(this);
    this.filterByRating = this.filterByRating.bind(this);
    this.generateFilterStateDropdown = this.generateFilterStateDropdown.bind(this);
    this.generateFilterRatingDropdown = this.generateFilterRatingDropdown.bind(this);
    this.generatePagination = this.generatePagination.bind(this);

    this.sortByAlphabetAZ = this.sortByAlphabetAZ.bind(this);
    this.sortByAlphabetZA = this.sortByAlphabetZA.bind(this);
    this.sortByIncomeAmount = this.sortByIncomeAmount.bind(this);
    this.sortByRating = this.sortByRating.bind(this);

    this.reset = this.reset.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
  }

  filterByState(state) {
    this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.state.includes(state));
    this.selectPage(1);
    this.setState ({
         sort_name: "Sort",
         state_filter_name : "Filter by State: " + state
    })
  }

  filterByRating(rating) {
    this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.rating == rating);
    this.selectPage(1);
    this.setState ({
         sort_name: "Sort",
         rating_filter_name: "Filter by Rating: " + rating
    })
  }

  filterByIncome(income) {
    var income_arr = [1000, 10000, 100000, 1000000, 10000000];
    var index = income_arr.indexOf(income);
    if(index == 4) {
        this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.incomeAmount >= income);
    } else {
        this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.incomeAmount >= income && entry.props.incomeAmount < income_arr[index + 1]);
    }
    this.selectPage(1);
    this.setState ({
         sort_name: "Sort",
         income_filter_name: "Filter by income: >" + income
    })
  }

  generateFilterStateDropdown() {
    var entries = new Set();
    for (var i in this.props.entries){
      entries.add(this.props.entries[i].state);
    }
    return [...entries].sort().map(entry => {
      return(
        <li class="dropdown-item" onClick={()=>{this.filterByState(entry)}}>{entry}</li>
      );
        });
  }

  generateFilterRatingDropdown() {
    var entries = new Set();
    for (var i in this.props.entries){
      entries.add(this.props.entries[i].rating);
    }
    return [...entries].sort().map(entry => {
      return(
        <li class="dropdown-item" onClick={()=>{this.filterByRating(entry)}}>{entry}</li>
      );
        });
  }

  generateFilterIncomeDropdown() {
    var entries = [1000, 10000, 100000, 1000000, 10000000];
    return [...entries].sort().map(entry => {
      return(
        <li class="dropdown-item" onClick={()=>{this.filterByIncome(entry)}}> ≥{entry}</li>
      );
        });
  }

  generatePagination() {
    var arr = Array.from({length: Math.ceil(this.state.sorted_items.length/9)}, (v, k) => k+1);
    return [...arr].map(entry => {
      return(
        <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(entry)}}>{entry}</a>
          </li>
      )
    })
  }

  sortByAlphabetAZ() {
    this.toggles.alphabet = !this.toggles.alphabet;
    if(this.toggles.alphabet){
      this.reset();
    }
    this.setState({
         sort_name: "Sort by Name Alphabetically A-Z"
    })
  }

  sortByAlphabetZA() {
  this.toggles.alphabet = !this.toggles.alphabet;
    if(!this.toggles.alphabet){
      var myData = [].concat(this.orig_items).reverse();

      this.setState({
        sort_name : "Sort by Name Alphabetically Z-A",
        sorted_items: myData,
        list_items : myData.slice(0,9),
        current_page : 1
      });
    }
  }

  sortByIncomeAmount() {
    var myData = [].concat(this.orig_items);
    this.toggles.incomeAmount = !this.toggles.incomeAmount;
    if(this.toggles.incomeAmount){
      myData.sort((a, b) => b.props.incomeAmount - a.props.incomeAmount);
    }
    else{
      myData.sort((a, b) => a.props.incomeAmount - b.props.incomeAmount);
    }

    this.setState({
      sort_name : "Sort by Income",
      sorted_items: myData,
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  sortByRating() {
    var myData = [].concat(this.orig_items);
    this.toggles.rating = !this.toggles.rating;
    if(this.toggles.rating){
      myData.sort((a, b) => b.props.rating - a.props.rating);
    }
    else{
      myData.sort((a, b) => a.props.rating - b.props.rating);
    }

    this.setState({
      sort_name : "Sort by Rating",
      sorted_items: myData,
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  reset() {
    this.setState({
      sort_name : "Sort",
      state_filter_name : "Filter by State",
      rating_filter_name: "Filter by Rating",
      sorted_items: this.orig_items,
      list_items: this.orig_items.slice(0,9),
      current_page : 1,
    })
  }

  handleNext() {
    if(this.state.current_page < Math.ceil(this.state.sorted_items.length/9)){
      this.selectPage(this.state.current_page + 1);
    }
  }

  handlePrevious() {
    if(this.state.current_page > 1){
      this.selectPage(this.state.current_page - 1);
    }

  }

  selectPage(page) {
    var end = page * 9;
    var start = end - 9;
    this.setState({
      current_page : page,
      list_items: this.state.sorted_items.slice(start,end)
    })
  }

  updateSearchValue(event) {
    this.setState({
      searchValue: event.target.value
    });
  }


  render() {

    return (
      <div className="container">
        <div class="form-row align-items-center">
          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.sort_name}
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <div class="dropdown-item" onClick={this.sortByAlphabetAZ}>By Alphabetical A-Z</div>
                <div class="dropdown-item" onClick={this.sortByAlphabetZA}>By Alphabetical Z-A</div>
                <div class="dropdown-item" onClick={this.sortByRating}>By Rating</div>
                <div class="dropdown-item" onClick={this.sortByIncomeAmount}>By Income</div>
              </div>
            </div>
          </div>

          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.state_filter_name}
              </button>
              <ul class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
                {this.generateFilterStateDropdown()}
              </ul>
            </div>
          </div>

          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.rating_filter_name}
              </button>
              <ul class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
                {this.generateFilterRatingDropdown()}
              </ul>
            </div>
          </div>

          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.income_filter_name}
              </button>
              <ul class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton">
                {this.generateFilterIncomeDropdown()}
              </ul>
            </div>
          </div>

          <div class="col-auto">
                <input type="search" class="form-control" id="search" placeholder="Enter Search Term"
                onChange={event => this.updateSearchValue(event)}
                onKeyPress={event => {if (event.key === "Enter") {location.replace("https://earth-first.live/orgs_search/" + this.state.searchValue)} }} />
          </div>

          <div class="col-auto">
            <button type="button" class="btn btn-danger" onClick={this.reset}>Reset</button>
          </div>

        </div>

        <br />

        <div className="card-columns">
           {this.state.list_items}
       </div>
       <ul class="pagination pg-blue justify-content-center">
       <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(1)}} >First</a>
          </li>
          <li class="page-item">
            <a class="page-link" onClick={this.handlePrevious}>Previous</a>
          </li>
          {this.generatePagination()}
          <li class="page-item">
            <a class="page-link" onClick={this.handleNext}>Next</a>
          </li>
          <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(11)}}>Last</a>
          </li>
        </ul>
      </div>

    );
  }
}

class OrgContact extends React.Component {
  render() {
    return (
      <div>
        <br />
        <br />
        <h2>Contact Information</h2>
          <div className="row_contact justify-content-md-center">
            <center>
              <b>{this.props.contact.location}</b>
              <br />
              {this.props.contact.street}
              <br />
              {this.props.contact.state}
              <br />
              {this.props.contact.country}
              <br />
              {this.props.contact.phone}
            </center>
          </div>
          <br />
          <div class="embed-responsive embed-responsive-21by9">
            <iframe width="600" height="500" src={"https://maps.google.com/maps?q="+this.props.contact.street + " " + this.props.contact.state + "&t=&z=13&ie=UTF8&iwloc=&output=embed"} frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"></iframe>
          </div>
      </div>
    );
  }
}


class OrgEntry extends React.Component {
  render() {
    return (
      <div
        className="card h-100 card-body"
        >
      <img
            src={this.props.logo_url}
            className="card-img-top"
          />
        
        <div className="card-body">
          <a href={this.props.url}>
          {this.props.content}
          </a>
          <br/>
          {this.props.city}, {this.props.state} <br/>
          Rating: {this.props.rating} <br/>
          Income: {this.props.incomeAmount}
        </div>
      </div>
    );
  }
}


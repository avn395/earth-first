class OrgAOI extends React.Component {
  render() {
    const data=this.props.states;
    const list_items=data.map((d, i)=> <OrgAoiEntry url={'/states/'+d} content={d} key={i}/>);
    return (
      <div>
        <br/>
        <br/>
        <h2>Areas of Influence</h2>
        { (list_items.length > 0) && <p>The {this.props.title} is located in the following states:</p> }
        { (list_items.length > 0) && <grid>
          <div className="row">
            {list_items}
          </div>
        </grid>}
      </div>
    );
  }
}

// Change these to cards
class OrgAoiEntry extends React.Component {
  render() {
    return (
      <div
        className="col"
        style={{
          backgroundColor: "silver"
        }}
      >
        <a href={this.props.url}>
         {this.props.content} 
        </a>
      </div>
    );
  }
}




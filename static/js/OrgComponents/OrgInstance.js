class OrgInstance extends React.Component {
  render() {
    return (
      <div className="container">
        <OrgInfo info={this.props.org_props.org_info}/>
        <OrgContact contact={this.props.org_props.org_contact}/>
        <OrgAOI states={this.props.org_props.org_aoi.states} title={this.props.org_props.org_info.title}/>
        <OrgPoli poli={this.props.org_props.org_poli} title={this.props.org_props.org_info.title}/>
      </div>
    );
  }
}


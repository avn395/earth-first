
class OrgInfo extends React.Component {
  render() {
    return (
      <div>
        <h1>
          {this.props.info.title}
        </h1>
        <img
          src={this.props.info.logo_url}
          className="img-fluid center"
          alt="Responsive image"
        />
        <br />
        <p>
          <h2>{this.props.info.tagline}</h2>
        </p>
        {(this.props.info.desc != null) && (<div className="card h-100 card-body">  
          <div className="card-body">
            {this.props.info.desc}
          </div>
        </div>)}
        <br />
        <div class="container">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe
            src={"https://www.youtube.com/embed/" + this.props.info.youtube_id} allow="autoplay; encrypted-media" allowFullScreen>
          </iframe>
        </div>
        </div>
        {/* <h2>Rating: {this.props.info.rating}</h2>
        <br />
        <br />
        <h2>Headquarters</h2>
        <p>
          The headquarters of the {this.props.info.title} is in {this.props.info.hq_location}.
        </p>
        <br /> */}
      </div>
    );
  }
}


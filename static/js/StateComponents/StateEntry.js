class StateEntry extends React.Component {
  render() {
    return (
      <div
        className="card"
        >
      <img
            src={this.props.logo_url}
            className="card-img-top"
          />
        
        <div className="card-body">
          <a href={this.props.url}>
          {this.props.content} | {this.props.abbr}
          </a>
          <div>
            Number of Bills: {this.props.num_bills} <br/>
            Number of Orgs: {this.props.num_orgs} <br/>
            Number of Programs: {this.props.num_progs} <br/>
            Region: {this.props.region}
          </div>
        </div>
      </div>
    );
  }
}



class StateCurbside extends React.Component {
  render() {
    return (
      <div>
        <h2
          style={{
            textAlign: "center"
          }}
        >
          Available Curbside Pick Up Programs
        </h2>
        <StateCurbsideEntry c_info={this.props.c_info}/>
        <br />
      </div>
    );
  }
}


class StateCurbsideEntry extends React.Component {
  render() {
    return (
      <div
        className="card mx-auto"
        style={{
          width: "400px"
        }}
      >
        <div className="card-header">
          <h4
            style={{
              margin: "auto"
            }}
          >
            {this.props.c_info.name}
          </h4>
        </div>
        <div className="card-body">
          {/* <img
            src={this.props.c_info.img_src}
            className="img-fluid"
            style={{
              maxHeight: "400px"
            }}
          /> */}
          <ul>
            <li>
              Available in {this.props.c_info.city}, {this.props.state}
            </li>
            <li>
              Hours of Operation: {this.props.c_info.hours}
            </li>
            <li>
              Contact us at: {this.props.c_info.phone}
            </li>
            <li>
              For more information, click <a href={this.props.c_info.url}>here</a>.
            </li>
          </ul>
        </div>
        <div className="card-footer">
         {/* {this.props.c_info.desc}  */}
        </div>
      </div>
    );
  }
}




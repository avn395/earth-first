
class StateInstance extends React.Component {
  render() {
    return (
      <div className="container">
        <StateInfo info={this.props.state_props.state_info}/>
        <StateComposting state={this.props.state_props.state_info.state} abbr={this.props.state_props.state_info.abbr} c_info={this.props.state_props.state_compost}/>
        <StateCurbside state={this.props.state_props.state_info.state} c_info={this.props.state_props.state_curb}/>
        <div class="embed-responsive embed-responsive-21by9">
          <iframe width="600" height="500" src={"https://maps.google.com/maps?q="+ this.props.state_props.state_info.state + "&t=&z=6&ie=UTF8&iwloc=&output=embed"} frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"></iframe>
        </div>
        <StatePoli state={this.props.state_props.state_info.state} p_info={this.props.state_props.state_poli}/>
        <StateOrg state={this.props.state_props.state_info.state} p_info={this.props.state_props.state_orgs}/>
      </div>
    );
  }
}


class StatePoli extends React.Component {
  render() {
    const poli_entries = this.props.p_info;
    const list_items = poli_entries.map((d, i)=><StatePoliEntry state={this.props.state} url={d.url} name={d.name} key={i}/>);

    return (
      <div>
        <br />
        <br />
        <h2>Politics</h2>
        { (list_items.length > 0) && <div>Environmental Bills Originiating in {this.props.state}:</div>}
        { (list_items.length > 0) && (<grid>
          <div className="row">
            {list_items}
          </div>
         </grid>)}
        {(list_items.length == 0) && <div>There are no Environmental Bills Originiating in {this.props.state}.</div>}
        <br />
       <br />
      </div>
    );
  }
}

class StatePoliEntry extends React.Component {
  render() {
    return (
      <div
        className="col-xl"
        style={{
          backgroundColor: "silver"
        }}
      >
        <a href={this.props.url}>
          {this.props.name.substring(0,50)}
        </a>
      </div>
    );
  }
}


class StateOrg extends React.Component {
  render() {
    const org_entries = this.props.p_info;
    const list_items = org_entries.map((d, i)=><StateOrgEntry state={this.props.state} url={d.url} name={d.name} key={i}/>);
    return (
      <div>
        <h2>Organizations</h2>
        {(list_items.length > 0) && <p>Environmental Organizations in {this.props.state}:</p> }
        {(list_items.length > 0) && <grid>
          <div className="row">
            {list_items}
          </div>
        </grid>}
        {(list_items.length == 0) && <div>There are no Environmental Organizations in {this.props.state}.</div>}
        <br />
        <br />
      </div>
    );
  }
}

class StateOrgEntry extends React.Component {
  render() {
    return (
      <div
        className="col-xl"
        style={{
          backgroundColor: "silver"
        }}
      >
        <a href={this.props.url}>
          {this.props.name.substring(0,30)}
        </a>
      </div>
    );
  }
}




class StateComposting extends React.Component {
  constructor(props){
    super(props);
    this.state = {list_items: []};
    $.get("https://earth-first.live/api/programs?composting=True&State=" + this.props.abbr, (data) => {
      var list_items = data.map((d, i) => <StateCompostingEntry c_info={d} state={this.props.state} key={i} />);
      this.setState({list_items: list_items}); 
    });
    
  }
  render() {
    return (
      <div>
        <br />
        <br />
        <h2>Available Composting Programs</h2>
        {this.state.list_items}
          {(this.state.list_items.length == 0) && (<StateCompostingEntry c_info={{
                City: "N/A",
                Hours: "N/A",
                Phone: "N/A",
                Url: "https://google.com",
                // Issue with unterminated string literal
                // desc: data.Description,
                Name: "We do not have any composting programs on record for this state."
                }} state={this.props.state}/>)}
        <br />
      </div>
    );
  }
}


class StateCompostingEntry extends React.Component {
  render() {
    return (
      <div
        className="card mx-auto"
        style={{
          width: "400px"
        }}
      >
        <div className="card-header">
          <h4
            style={{
              margin: "auto"
            }}
          >
            {this.props.c_info.Name}
          </h4>
        </div>
        <div className="card-body">
          {/* <img
            src="https://www.utdallas.edu/ehs/images/icon_environment.png"
            className="img-fluid"
            style={{
              maxHeight: "400px"
            }}
          /> */}
          <ul>
            <li>
              Available in {this.props.c_info.City}, {this.props.state}
            </li>
            <li>
             {this.props.c_info.Hours} 
            </li>
            <li>
              Contact us at: {this.props.c_info.Phone} 
            </li>
            <li>
              For more information, click <a href={this.props.c_info.Url}>here</a>.
            </li>
          </ul>
        </div>
        <div className="card-footer">
         {this.props.c_info.Description} 
        </div>
      </div>
    );
  }
}




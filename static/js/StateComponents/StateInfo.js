
class StateInfo extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.info.state}</h1>
        <img
          src={this.props.info.map_src}
          className="img-fluid center"
          alt="Responsive image"
        />
      </div>
    );
  }
}


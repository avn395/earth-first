// import OrgEntry from "./OrgEntry";
class StateEntries extends React.Component {
  constructor(props) {
    super(props);
    this.orig_items = this.props.entries.map((d, i) => <StateEntry url={d.url} content={d.content} abbr={d.abbr} logo_url={d.logo_url} num_bills={d.num_bills} num_orgs={d.num_orgs} num_progs={d.num_progs} region={d.region} key={i} />);
    this.toggles = {
      num_progs : false,
      num_orgs: false,
      num_bills: false,
    };

    this.state = {
      sort_name: "Sort",
      region_filter_name: "Filter by Region",
      bills_filter_name: "Filter by # Bills",
      progs_filter_name: "Filter by # Programs",
      sorted_items: this.orig_items,
      list_items: this.orig_items.slice(0,9),
      current_page : 1,
      searchValue: '',
    };

    // Pagination
    this.generatePagination = this.generatePagination.bind(this);

    // Filtering
    this.filterByRegion = this.filterByRegion.bind(this);
    this.filterByNumBills = this.filterByNumBills.bind(this);
    this.filterByNumPrograms = this.filterByNumPrograms.bind(this);
    this.generateFilterRegionDropdown = this.generateFilterRegionDropdown.bind(this);
    this.generateFilterBillsDropdown = this.generateFilterBillsDropdown.bind(this);
    this.generateFilterProgramsDropdown = this.generateFilterProgramsDropdown.bind(this);

    // Sorting
    this.sortByNumProgs = this.sortByNumProgs.bind(this);
    this.sortByAlphabetAZ = this.sortByAlphabetAZ.bind(this);
    this.sortByAlphabetZA = this.sortByAlphabetZA.bind(this);
    this.sortByNumOrgs = this.sortByNumOrgs.bind(this);
    this.sortByNumBills = this.sortByNumBills.bind(this);

    // Misc Buttons
    this.reset = this.reset.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
  }

  filterByRegion(region) {
    this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.region.includes(region));
    this.selectPage(1);
    this.setState({
         sort_name : "Sort",
         region_filter_name : "Filter by Region: " + region
    });
  }

  filterByNumBills(num_bills) {
    this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.num_bills == num_bills);
    this.selectPage(1);
    this.setState({
         sort_name : "Sort",
         bills_filter_name : "Filter by # Bills: " + num_bills
    });
  }

  filterByNumPrograms(num_progs) {
       this.state.sorted_items = this.state.sorted_items.filter(entry => entry.props.num_progs == num_progs);
       this.selectPage(1);
       this.setState({
            sort_name: "Sort",
            progs_filter_name: "Filter by # Programs: " + num_progs
       })
 }

  generateFilterRegionDropdown() {
    var entries = new Set();
    for (var i in this.props.entries){
      entries.add(this.props.entries[i].region);
    }
    return [...entries].map(entry => {
      return(
        <div class="dropdown-item" onClick={()=>{this.filterByRegion(entry)}}>{entry}</div>
      );
        });
  }

  generateFilterBillsDropdown() {
    var entries = new Set();
    for (var i in this.props.entries){
      entries.add(this.props.entries[i].num_bills);
    }
    return [...entries].sort().map(entry => {
      return(
        <div class="dropdown-item" onClick={()=>{this.filterByNumBills(entry)}}>{entry}</div>
      );
        });
  }

  generateFilterProgramsDropdown() {
    var entries = new Set();
    for (var i in this.props.entries){
      entries.add(this.props.entries[i].num_bills);
    }
    return [...entries].sort().map(entry => {
      return(
        <div class="dropdown-item" onClick={()=>{this.filterByNumPrograms(entry)}}>{entry}</div>
      );
        });
  }

  generatePagination() {
    var arr = Array.from({length: Math.ceil(this.state.sorted_items.length/9)}, (v, k) => k+1);
    return [...arr].map(entry => {
      return(
        <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(entry)}}>{entry}</a>
          </li>
      )
    })
  }

  sortByNumProgs() {
    var myData = [].concat(this.orig_items);
    this.toggles.num_progs = !this.toggles.num_progs;
    if(this.toggles.num_progs){
      myData.sort((a, b) => a.props.num_progs - b.props.num_progs);
    }
    else{
      myData.sort((a, b) => b.props.num_progs - a.props.num_progs);
    }

    this.setState({
      sort_name : "Sort by # programs",
      sorted_items: myData,
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  sortByNumOrgs() {
    var myData = [].concat(this.orig_items);
    this.toggles.num_orgs = !this.toggles.num_orgs;
    if(this.toggles.num_orgs){
      myData.sort((a, b) => a.props.num_orgs - b.props.num_orgs);
    }
    else{
      myData.sort((a, b) => b.props.num_orgs - a.props.num_orgs);
    }

    this.setState({
      sort_name: "Sort by # organizations",
      sorted_items: myData,
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  sortByNumBills() {
    var myData = [].concat(this.orig_items);
    this.toggles.num_bills = !this.toggles.num_bills;
    if(this.toggles.num_bills){
      myData.sort((a, b) => a.props.num_bills - b.props.num_bills);
    }
    else{
      myData.sort((a, b) => b.props.num_bills - a.props.num_bills);
    }

    this.setState({
      sort_name: "Sort by # bills",
      sorted_items: myData,
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  sortByAlphabetAZ() {
    this.reset();
    this.setState({
      sort_name : "Sort Alphabetically"
    });
  }

  sortByAlphabetZA() {
    var myData = [].concat(this.orig_items).reverse();

    this.setState({
      sort_name : "Sort Alphabetically Z-A",
      sorted_items: myData,
      list_items : myData.slice(0,9),
      current_page : 1
    });
  }

  reset() {
    this.setState({
      sorted_items: this.orig_items,
      sort_name: "Sort",
      region_filter_name: "Filter by Region",
      bills_filter_name: "Filter by # Bills",
      progs_filter_name: "Filter by # Programs",
      list_items: this.orig_items.slice(0,9),
      current_page : 1,
    })
  }

  handleNext() {
    if(this.state.current_page < 6){
      this.selectPage(this.state.current_page + 1);
    }
  }

  handlePrevious() {
    if(this.state.current_page > 1){
      this.selectPage(this.state.current_page - 1);
    }
  }

  selectPage(page) {
    var end = page * 9;
    var start = end - 9;
    this.setState({
      current_page : page,
      list_items: this.state.sorted_items.slice(start,end)
    })
  }

  updateSearchValue(event) {
    this.setState({
      searchValue: event.target.value
    });
  }

  render() {
    return (
      <div className="container">
        <div class="form-row align-items-center">
          <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.sort_name}
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <div class="dropdown-item" onClick={this.sortByAlphabetAZ}>By Alphabetical A-Z</div>
                <div class="dropdown-item" onClick={this.sortByAlphabetZA}>By Alphabetical Z-A</div>
                <div class="dropdown-item" onClick={this.sortByNumBills}>By Number of Bills</div>
                <div class="dropdown-item" onClick={this.sortByNumOrgs}>By Number of Organizations</div>
                <div class="dropdown-item" onClick={this.sortByNumProgs}>By Number of Programs</div>

              </div>
            </div>
          </div>
            <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.region_filter_name}
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                {this.generateFilterRegionDropdown()}
              </div>
            </div>
          </div>

            <div class="col-auto">
            <div class="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.state.bills_filter_name}
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                {this.generateFilterBillsDropdown()}
              </div>
            </div>
          </div>

          <div class="col-auto">
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {this.state.progs_filter_name}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              {this.generateFilterProgramsDropdown()}
            </div>
          </div>
        </div>

        <div class="col-auto">
                <input type="search" class="form-control" id="search" placeholder="Enter Search Term"
                onChange={event => this.updateSearchValue(event)}
                onKeyPress={event => {if (event.key === "Enter") {location.replace("https://earth-first.live/states_search/" + this.state.searchValue)} }} />
          </div>

          <div class="col-auto">
            <button type="button" class="btn btn-danger" onClick={this.reset}>Reset</button>
          </div>

        </div>

        <br />

        <div className="card-columns">
          {this.state.list_items}
        </div>

        <ul class="pagination pg-blue justify-content-center">
        <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(1)}} >First</a>
          </li>
          <li class="page-item">
            <a class="page-link" onClick={this.handlePrevious}>Previous</a>
          </li>
          {this.generatePagination()}
          <li class="page-item">
            <a class="page-link" onClick={this.handleNext}>Next</a>
          </li>
          <li class="page-item">
            <a class="page-link" onClick={()=>{this.selectPage(this.num_pages)}}>Last</a>
          </li>
        </ul>
      </div>
    );
  }
}

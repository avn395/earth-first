// import React from "react";

class UsefulLinks extends React.Component {
  render() {
    return (
      <div className="container">
        <h1>Resources</h1>
        <div className="Resources">
          <center>
            <a href="https://en.wikipedia.org/w/api.php">Wikipedia API</a>
            <br />
            We used the Wikipedia API to get an XML page for descriptions of our
            organization instances.
            <br />
            <a href="https://projects.propublica.org/api-docs/congress-api/">
              Propublica.org API
            </a>
            <br />
            We used the Propublic API to search for bills related to the
            environment and extract a JSON.
            <br />
            <a href="https://www.epa.gov/environmental-topics/greener-living">
              EPA API
            </a>
            <br />
            We have not used this API yet, but we plan to use it in future
            phases.
            <br />
            <a href="https://earth911.com/">Earth911 API</a>
            <br />
            We used the Earth911 API to find location specific information about
            curbside pick up and composting programs.
            <br />
            <a href="https://gitlab.com/">Gitlab API</a>
            <br />
            We used the GitLab API to dynamically update commits and issues.
            <br />
          </center>
        </div>
        <h1>Tools</h1>
        <div className="Tools">
          <center>
            <a href="https://aws.amazon.com/">AWS </a>
            <br />
            We used AWS to host the webpage.
            <br />
            <a href="http://flask.pocoo.org/">Flask </a>
            <br />
            We used Flask to call other APIs and render our pages.
            <br />
            <a href="https://www.getpostman.com/">Postman </a>
            <br />
            We used Postman to create our Restful API.
            <br />
            <a href="https://getbootstrap.com/docs/4.3/getting-started/introduction/">
              Bootstrap
            </a>
            <br />
            We used Bootstrap for frontend features like carousel, cards, and
            grid.
            <br />
            <a href="https://www.overleaf.com/">Overleaf </a>
            <br />
            We used OverLeaf to write our technical report.
            <br />
          </center>
        </div>
        <h1>Links</h1>
        <div className="Links">
          <center>
            <a href="https://gitlab.com/avn395/earth-first">
              {" "}
              GitLab Repository
            </a>
            <br />
            <a href="https://documenter.getpostman.com/view/6608495/S1ERwxNg">
              {" "}
              Postman{" "}
            </a>
            <br />
          </center>
        </div>
      </div>
    );
  }
}
ReactDOM.render(<UsefulLinks/>, document.getElementById('UsefulLinks'));
// export default usefulLinks;

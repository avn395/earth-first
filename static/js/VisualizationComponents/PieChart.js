$.get("https://api.inneedofsoup.xyz/api/food", renderItems);
function renderItems(response) {
    var data = response.objects;
    var totalCarbs = 0;
    var totalFat = 0;
    var totalProtein = 0;
    var totalCount = 0;		//calcuting total manually

    data.forEach(element => {
        totalCarbs += element.carbs_per_100g;
        totalFat += element.fat_per_100g;
        totalProtein += element.protein_per_100g;
        totalCount += element.calories_per_100g;
    });

    totalCarbs /= data.length;
    totalFat /= data.length;
    totalProtein /= data.length;
    totalCount /= data.length;

    data = [{name: "Carbs", count: totalCarbs, color:"#8ABD4A"}, {name: "Fat", count: totalFat, color:"#f8b70a"} ,
    {name: "Protein", count: totalProtein, color:"9f8170"}, {name: "Calories", count: totalCount, color:"#6149c6"}];

    var width = 540,
        height = 540,
        radius = 200;

    var arc = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(100);

    var pie = d3.pie()
        .sort(null)
        .value(function (d) {
            return Math.ceil(d.count) + 2;
        });
    var svg = d3.select("#pieChart")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var g = svg.selectAll(".arc")
        .data(pie(data))
        .enter().append("g");

    g.append("path")
        .attr("d", arc)
        .style("fill", function (d, i) {
            return d.data.color;
        });

    g.append("text")
        .attr("transform", function (d) {
            var _d = arc.centroid(d);
            _d[0] *= 1.5;	//multiply by a constant factor
            _d[1] *= 1.5;	//multiply by a constant factor
            return "translate(" + _d + ")";
        })
        .attr("dy", ".50em")
        .style("text-anchor", "middle")
        .text(function (d) {
            return d.data.name + ': ' +  d.data.count.toFixed(2) + ' g';
        });
}

class IncomeHistCustomer extends React.Component {
    constructor(props){
        super(props);
        }
        
        drawChart() {
            $.get("https://api.inneedofsoup.xyz/api/donation_opportunity", this.drawChartb);
        }
      
    drawChartb(response) {
            response = response.objects;
            
            var dict = {0:0, 1000: 0, 10000:0, 100000:0, 1000000:0, 10000000:0};
            for(var i = 0; i < response.length; i++) {
                var org = response[i];
                var income = org.income;
                if (income < 1000) {
                    dict[0]++;
                } else if (income < 10000) {
                    dict[1000]++;
                } else if (income < 100000) {
                    dict[10000]++;
                } else if (income < 1000000) {
                    dict[100000]++;
                } else if (income < 10000000) {
                    dict[1000000]++;
                } else if (income >=10000000) {
                    dict[10000000]++;
                }
            }

      var data = [dict[0], dict[1000], dict[10000], dict[100000], dict[1000000], dict[10000000]];

            var height = 400;
            var width = 600;
            var barPadding = 2;
            var barWidth = (width / data.length) - barPadding;
            var margin = 40;

            var yScale = d3.scaleLinear()
                .domain([0, d3.max(data)])
                .range([0+margin, height-margin]);
            var xScale = d3.scaleBand()
                .domain(["0-1000", "1000-10000", "10000-100000", "100000-1000000", "1000000-10000000", "10000000-inf"])
                .range([0+margin, width-margin])
                .paddingInner(.05);

            var svg = d3.select("#chartArea3")
                .style('width', width + 'px')
                .style('height', height + 'px');


            svg.selectAll('rect')
                .data(data)
                .enter()
                .append('rect')
                .attr('class', 'bar')
                .attr("x", function (d, i) {
                    return xScale(xScale.domain()[i]);
                })
                .attr("y", function (d, i) {
                    return height-margin;
                })
                .attr("width", function (d, i) {
                    return xScale.bandwidth();
                })
                .attr("fill", function (d, i) {
                    return 'rgb(' + i * (200/6) + ', ' + 200 + ', ' + i * (200/6) + ')';
                })
                .attr("height", 0)
                .transition()
                .duration(500)
                .delay(function (d, i) {
                    return i * 100;
                })
                .attr("y", function (d, i) {
                    return height - yScale(d);
                })
                .attr("height", function (d, i) {
                    return yScale(d)-40;
                });

            svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 35)
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "underline")  
                .text("Number of Donation Opportunities by Income");
                
            var x_axis = d3.axisBottom()
                .scale(xScale);
            var yScale2 = d3.scaleLinear()
                .domain([0, d3.max(data)])
                .range([height-margin, margin]);
            var y_axis = d3.axisLeft()
                .scale(yScale2);

            svg.append("g")
                .attr("transform", "translate(0, 360)")
                .call(x_axis);
            svg.append("g")
                .attr("transform", "translate(40,0)")
                .call(y_axis);
            return "tempchartArea3";
    }
          
    render(){
      return <div id={"thefinalone3"}>
                <div id={this.drawChart()}></div>
            </div>
    }
  }
  ReactDOM.render(<IncomeHistCustomer/>, document.getElementById('IncomeHistCustomer'));

class PartiesVis extends React.Component {
    constructor(props){
        super(props);
		}

		drawChart() {
			$.get("https://earth-first.live/api/bills", this.drawChartb);
		}

    drawChartb(response) {

			var dict = {"R": 0, "D":0, "I":0};
			for(var i = 0; i < response.length; i++) {
				var bill = response[i];
				var party = bill.sponsor_party;
				dict[party]++;
			}

      var data = [dict["R"], dict["D"], dict["I"]];

			var height = 400;
			var width = 600;
			var barPadding = 2;
			var barWidth = (width / data.length) - barPadding;
			var margin = 40;

			var yScale = d3.scaleLinear()
				.domain([0, d3.max(data)])
				.range([0+margin, height-margin]);

			var xScale = d3.scaleBand()
				.domain(["R", "D", "I"])
				.range([0+margin, width-margin])
				.paddingInner(.05);


			var svg = d3.select("#chartArea")
				.style('width', width + 'px')
				.style('height', height + 'px');


			svg.selectAll('rect')
				.data(data)
				.enter()
				.append('rect')
				.attr('class', 'bar')
				.attr("x", function (d, i) {
					return xScale(xScale.domain()[i]);
				})
				.attr("y", function (d, i) {
					return height-margin;
				})
				.attr("width", function (d, i) {
					return xScale.bandwidth();
				})
				.attr("fill", function (d, i) {
					if(i == 1){
						return 'rgb(0, 0, 200)';
					}
					else if (i == 2){
						return 'rgb(0, 200, 0)';
					}
					else{
						return 'rgb(256, ' + Math.round(i * (256/3) / 2) + ', ' + i * (256/3) + ')';
					}
					
				})
				.attr("height", 0)
				.transition()
				.duration(500)
				.delay(function (d, i) {
					return i * 100;
				})
				.attr("y", function (d, i) {
					return height - yScale(d);
				})
				.attr("height", function (d, i) {
					return yScale(d)-40;
				});

			svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 35)
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "underline")
				.text("Number of Bills by Sponsor Party");

			var x_axis = d3.axisBottom()
				.scale(xScale);
			var yScale2 = d3.scaleLinear()
				.domain([0, d3.max(data)])
				.range([height-margin, margin]);
			var y_axis = d3.axisLeft()
				.scale(yScale2);

			svg.append("g")
				.attr("transform", "translate(0, 360)")
				.call(x_axis);
			svg.append("g")
				.attr("transform", "translate(40,0)")
				.call(y_axis);
			return "tempchartArea";
    }

    render(){
      return <div id={"thefinalone"}>
				<div id={this.drawChart()}></div>
			</div>
    }
	}
	ReactDOM.render(<PartiesVis/>, document.getElementById('PartiesVis'));

var response = [];
$.get("https://api.inneedofsoup.xyz/api/soup_kitchen?page=1", (data) =>{
    data.objects.forEach(element => {
        response.push([element.longitude, element.latitude]);
    });
    $.get("https://api.inneedofsoup.xyz/api/soup_kitchen?page=2", (data) =>{
        data.objects.forEach(element => {
            response.push([element.longitude, element.latitude]);
        });
            $.get("https://api.inneedofsoup.xyz/api/soup_kitchen?page=3", (data) =>{
            data.objects.forEach(element => {
                response.push([element.longitude, element.latitude]);
            });
            renderItems(response);
            });
        });
});

function renderItems(response){
    var data = response;
    var width = 1200,
    height = 550;

    
// set projection
var projection = d3.geoMercator();

// create path variable
var path = d3.geoPath()
    .projection(projection);

d3.json("https://gist.githubusercontent.com/mbostock/4090846/raw/d534aba169207548a8a3d670c9c2cc719ff05c47/us.json", function(error, topo) { console.log(topo);
  	var states = topojson.feature(topo, topo.objects.states).features

  	// set projection parameters
  	projection
      .scale(1000)
      .center([-106, 37.5])

    // create svg variable
    var svg2 = d3.select("#map")
    				.attr("width", width)
    				.attr("height", height);

	// add states from topojson
	svg2.selectAll("path")
      .data(states).enter()
      .append("path")
      .attr("class", "feature")
      .style("fill", "green")
      .attr("d", path);

    // put boarder around states 
  	svg2.append("path")
      .datum(topojson.mesh(topo, topo.objects.states, function(a, b) { return a !== b; }))
      .attr("class", "mesh")
      .attr("d", path);

    // add circles to svg
    svg2.selectAll("circle")
		.data(data).enter()
		.append("circle")
		.attr("cx", function (d) { console.log(projection(d)); return projection(d)[0]; })
		.attr("cy", function (d) { return projection(d)[1]; })
		.attr("r", "4px")
		.attr("fill", "black")

});

}

function tooltipHtml(n, d){	/* function to create html content string in tooltip div. */
    return "<h4>"+n+"</h4><table>"+
      "<tr><td>Bills </td><td>"+(d.bills)+"</td></tr>"+
      "<tr><td>Programs </td><td>"+(d.progs)+"</td></tr>"+
      "<tr><td>Organizations </td><td>"+(d.orgs)+"</td></tr>"+
      "</table>";
  }
  $.get( "https://earth-first.live/api/states", renderItems);
  function renderItems(data){
    var sampleData = {};
    data.forEach(function(d)
    {
        var orgs=d.num_orgs, 
        bills=d.num_bills, 
        progs=d.num_progs,
        name=d.abbr;
        sampleData[name]={bills:bills, orgs:orgs, 
          progs:progs, color:d3.interpolate("white", "green")( (bills+progs+orgs) / Math.max(10, bills+progs+orgs))};
    });
    ["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
    "ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH", 
    "MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT", 
    "CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN", 
    "WI", "MO", "AR", "OK", "KS", "LS", "VA"].forEach(d => {
      if (sampleData[d] == null){
        sampleData[d] = sampleData["MD"]
      }
    });
    uStates.draw("#statesvg", sampleData, tooltipHtml);
    d3.select(self.frameElement).style("height", "600px"); 
  }

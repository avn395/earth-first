// import React from "react";

class Statistics extends React.Component {
  render() {
    return (
      <div>
        <h2 className="Header"> Statistics</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th />
              <th>Commits</th>
              <th>Issues</th>
              <th>Unit Tests</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Total</th>
              <td id="total_commits">0</td>
              <td id="total_issues">0</td>
              <td id="total_unit_tests">100</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
ReactDOM.render(<Statistics/>, document.getElementById('StatisticsTable'));
// export default Statistics;

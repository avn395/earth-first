class Carousel extends React.Component {
    render() {
        return (
//   <!-- Carousel code taken from: https://getbootstrap.com/docs/4.0/components/carousel/ -->
        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
        <ol className="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div className="carousel-inner">
            <div className="carousel-item active">
            <img className="d-block w-100" src="https://climateprotection.org/wp-content/uploads/2017/10/congress006-1-1030x573.jpg" alt="First slide" style={{maxHeight:'512px', maxWidth:'1024px', margin:'auto'}} />
            <div className="carousel-caption d-none d-md-block">
            <h5>
                <a href="/politics" style={{color: 'white'}}>Politics</a>
            </h5>
            <p>Learn about bills affecting the environment.</p>
            </div>
            </div>
            <div className="carousel-item">
            <img className="d-block w-100" src="https://www.telegraph.co.uk/content/dam/Travel/2018/January/sydney-best-GETTY.jpg?imwidth=1400" alt="Second slide" style={{maxHeight:'512px', maxWidth:'1024px', margin:'auto'}}/>
            <div className="carousel-caption d-none d-md-block">
                <h5>
                <a href="/states" style={{color: 'white'}}>States</a>
                </h5>
            <p>Learn about how you can make a difference in your state.</p>
            </div>
            </div>
            <div className="carousel-item">
            <img className="d-block w-100" src="https://iredell.ces.ncsu.edu/wp-content/uploads/2017/10/Gardening.jpg" alt="Third slide" style={{maxHeight:'512px', maxWidth:'1024px', margin:'auto'}}/>
            <div className="carousel-caption d-none d-md-block">
                <h5>
                <a href="/orgs" style={{color: 'white'}}>Organizations</a>
                </h5>
            <p>Learn about environmental organizations.</p>
            </div>
            </div>
        </div>
        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
        </a>
        </div>
        );
    }
}
ReactDOM.render(<Carousel/>, document.getElementById('EFCarousel'));

// import React from "react";

class Profile1 extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="CardDeck card-deck">
          <div
            className="card"
            style={{
              width: "200px"
            }}
          >
            <div className="card-header">
              <h3
                style={{
                  margin: "auto"
                }}
              >
                Yaswin Veluguleti
              </h3>
            </div>
            <div className="card-body">
              {" "}
              <img
                src="../static/res/AboutMe_Yaswin.png"
                className="img-fluid"
              />{" "}
            </div>
            <div className="card-footer">
              <ul
                style={{
                  listStyleType: "square"
                }}
              >
                <li>Year: Sophomore, Graduates 2021</li>
                <li>Bio: Video games on one monitor, “work” on the other.</li>
                <li id="yaswin_commits">Commits:</li>
                <li id="yaswin_open">Issues Opened:</li>
                <li id="yaswin_closed">Issues Closed:</li>
                <li>Unit tests: 15</li>
                <li>Major Responsibilities: AWS</li>
              </ul>
            </div>
          </div>
          <div
            className="card"
            style={{
              width: "200px"
            }}
          >
            <div className="card-header">
              <h3
                style={{
                  margin: "auto"
                }}
              >
                Michael Lee
              </h3>
            </div>
            <div className="card-body">
              {" "}
              <img
                src="../static/res/AboutMe_Michael.jpeg"
                className="img-fluid"
              />{" "}
            </div>
            <div className="card-footer">
              <ul
                style={{
                  listStyleType: "square"
                }}
              >
                <li>Year: Sophomore, Graduates 2020</li>
                <li>Bio: Actually a senior by credit hours, ;)</li>
                <li id="michael_commits">Commits:</li>
                <li id="michael_open">Issues Opened:</li>
                <li id="michael_closed">Issues Closed:</li>
                <li>Unit tests: 40</li>
                <li>Major Responsibilities: Python/JS</li>
              </ul>
            </div>
          </div>
          <div
            className="card"
            style={{
              width: "200px"
            }}
          >
            <div className="card-header">
              <h3
                style={{
                  margin: "auto"
                }}
              >
                Rizwan Lubis
              </h3>
            </div>
            <div className="card-body">
              {" "}
              <img src="../static/res/AboutMe_Riz.png" className="img-fluid" />
            </div>
            <div className="card-footer">
              <ul
                style={{
                  listStyleType: "square"
                }}
              >
                <li>Year: Junior, Graduates 2019</li>
                <li>Bio: Junior Computer Science major who likes dogs</li>
                <li id="riz_commits">Commits:</li>
                <li id="riz_open">Issues Opened:</li>
                <li id="riz_closed">Issues Closed:</li>
                <li>Unit tests: 15</li>
                <li>Major Responsibilities: Python/HTML/CSS</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
ReactDOM.render(<Profile1/>, document.getElementById('Profile1row'));
// export default Profile1;

class Search extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            results: this.props.results.map((d, i) => <SearchCard url={d.url} title={d.title} image={d.image} text={d.text} search_term={d.search_term} key={i}/>),
        }
        this.noResults = this.state.results.length <= 0;
        this.noResultsMessage = (<div className="card h-100 card-body"><div class="card-body"> There were no results for <b>{this.props.search_term}</b> :(</div></div>);
        this.resultsMessage = (<div className="card h-100 card-body"><div class="card-body"> Returning results for <b>{this.props.search_term}</b>:</div></div>);
    }
    // Props: Link, Title, Image, Relevant Text, Search Term
    render(){
        return (
        <div class="container">
            <br/>
            <br />
            <ul>
                {(this.noResults ? this.noResultsMessage : this.resultsMessage)}
                {this.state.results}
            </ul>
        </div>
        );
    }
}

class GeneralSearchDropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display_name: 'Results from Politics',
        };
    }
    // Props: Link, Title, Image, Relevant Text, Search Term
    render() {
        return (
            <div class="container">
                <br />
                <br />
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {this.state.display_name}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="dropdown-item" onClick={() => this.setState({ display_name: "Results from Orgs" })}>Results from Orgs</div>
                        <div class="dropdown-item" onClick={() => this.setState({ display_name: "Results from Politics" })}>Results from Politics</div>
                        <div class="dropdown-item" onClick={() => this.setState({ display_name: "Results from States" })}>Results from States</div>
                    </div>
                </div>
                {this.state.display_name == 'Results from Orgs' && <Search results={this.props.orgs_results} search_term={this.props.search_term}/>}
                {this.state.display_name == 'Results from Politics' && <Search results={this.props.politics_results} search_term={this.props.search_term}/>}
                {this.state.display_name == 'Results from States' && <Search results={this.props.states_results} search_term={this.props.search_term}/>}
            </div>
        );
    }
}

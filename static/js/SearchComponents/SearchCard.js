class SearchCard extends React.Component{
    // Props: Url, Title, Image, Relevant Text, Search Term
    render(){
        return (
        <div className="card h-100 card-body">
        
        <div className="card-body">
          <a href={this.props.url}>
          {this.props.title}
          </a> 
          <br/>
          <SearchContext text={this.props.text} search_term={this.props.search_term} /><br/>
        </div>
      </div>
        );
    }
}

class SearchContext extends React.Component{
    constructor(props){
        super(props);
    }

    getHighlightedText(text, highlight) {
        // Split on higlight term and include term into parts, ignore case
        let parts = text.split(new RegExp(`(${highlight})`, 'gi'));
        return <span> { parts.map((part, i) => 
            <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold' , 'backgroundColor': 'yellow'} : {} }>
                { part }
            </span>)
        } </span>;
    }

    render(){
        return (
        <div>
            <p>
                {this.getHighlightedText(this.props.text,this.props.search_term)}
            </p>
        </div>
        );
    }
}

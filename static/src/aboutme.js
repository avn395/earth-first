
const IssuesUrl = "https://gitlab.com/api/v4/projects/10950119/issues";
const CommitsUrl = "https://gitlab.com/api/v4/projects/10950119/repository/commits";

function renderGitlabCommits(data, status) {

    var yasce = document.getElementById("yaswin_commits");
    var michce = document.getElementById("michael_commits");
    var rizce = document.getElementById("riz_commits");
    var avce = document.getElementById("avni_commits");
    var rilce = document.getElementById("ril_commits");
    var totalce = document.getElementById("total_commits");

    // Used to assert that only us 5 are making commits to the repo
    const maintainer_names = new Set(["Yaswin Veluguleti", "michael", "Rizwan Ahmad Lubis", "Rileigh", "avnandu"]);
    var commits_freq = {"Yaswin Veluguleti" : 0, "michael" : 0, "Rizwan Ahmad Lubis" : 0, "Rileigh" : 0, "avnandu" : 0};
    for(var i = 0; i < data.length; i++) {
        var name = data[i].author_name;
        if(!(maintainer_names.has(name))) continue;
        commits_freq[name] ++;
    }
    
    yasce.innerHTML = "Commits: "+ commits_freq["Yaswin Veluguleti"];
    michce.innerHTML = "Commits: "+ commits_freq["michael"];
    rizce.innerHTML = "Commits: "+ commits_freq["Rizwan Ahmad Lubis"];
    avce.innerHTML = "Commits: "+ commits_freq["avnandu"];
    rilce.innerHTML = "Commits: "+ commits_freq["Rileigh"];
    totalce.innerHTML = "Commits: " + (commits_freq["Yaswin Veluguleti"] 
                                     + commits_freq["michael"] 
                                     + commits_freq["Rizwan Ahmad Lubis"] 
                                     + commits_freq["avnandu"]
                                     + commits_freq["Rileigh"]);
}

function renderGitlabIssues(data, status) {
    var yasoe = document.getElementById("yaswin_open");
    var michoe = document.getElementById("michael_open");
    var rizoe = document.getElementById("riz_open");
    var avoe = document.getElementById("avni_open");
    var riloe = document.getElementById("ril_open");

    var yasce = document.getElementById("yaswin_closed");
    var michce = document.getElementById("michael_closed");
    var rizce = document.getElementById("riz_closed");
    var avce = document.getElementById("avni_closed");
    var rilce = document.getElementById("ril_closed");

    var totalie = document.getElementById("total_issues");

    // Only track the issues that us 5 are opening, members of other groups may add issues later on and we'll just ignore them
    const maintainer_usernames = new Set(["yaswin", "michaellee4", "ralubis", "rileighbandy", "avn395"]);
    var issues_opened = {"yaswin" : 0, "michaellee4" : 0, "ralubis" : 0, "rileighbandy" : 0,"avn395":0 };
    var issues_closed = {"yaswin" : 0, "michaellee4" : 0, "ralubis" : 0, "rileighbandy" : 0,"avn395":0 };
    for(var i = 0; i < data.length; i++) {
        var name = data[i].author.username;
        if(!(maintainer_usernames.has(name))) continue;
        var closed = !(data[i].closed_by === null);
        issues_opened[name] ++;
        if (closed)
        {
            var closed_username = data[i].closed_by.username;
            issues_closed[closed_username] ++;
        }
    }

    yasoe.innerHTML = "Issues Opened: " + issues_opened["yaswin"];
    michoe.innerHTML = "Issues Opened: " + issues_opened["michaellee4"];
    rizoe.innerHTML = "Issues Opened: " + issues_opened["ralubis"];
    avoe.innerHTML = "Issues Opened: " + issues_opened["avn395"];
    riloe.innerHTML = "Issues Opened: " + issues_opened["rileighbandy"];

    yasce.innerHTML = "Issues Closed: "+issues_closed["yaswin"];
    michce.innerHTML = "Issues Closed: "+issues_closed["michaellee4"];
    rizce.innerHTML = "Issues Closed: "+issues_closed["ralubis"];
    avce.innerHTML = "Issues Closed: "+issues_closed["avn395"];
    rilce.innerHTML = "Issues Closed: "+issues_closed["rileighbandy"];

    totalie.innerHTML = "" + (issues_opened["yaswin"] 
                                         + issues_opened["michaellee4"]
                                         + issues_opened["ralubis"]
                                         + issues_opened["avn395"] 
                                         + issues_opened["rileighbandy"]);
}

function countPage(data, commits_freq)
{
    const maintainer_names = new Set(["Yaswin Veluguleti", "michael", "Rizwan Ahmad Lubis", "Rileigh", "avnandu"]);
    for(var i = 0; i < data.length; i++) {
        var name = data[i].author_name;
        if(!(maintainer_names.has(name))) continue;
        commits_freq[name] ++;
    }
}

function updateCommitHTML(commits_freq)
{
    var yasce = document.getElementById("yaswin_commits");
    var michce = document.getElementById("michael_commits");
    var rizce = document.getElementById("riz_commits");
    var avce = document.getElementById("avni_commits");
    var rilce = document.getElementById("ril_commits");
    var totalce = document.getElementById("total_commits");
	totalce.innerHTML = "" + (commits_freq["Yaswin Veluguleti"] + commits_freq["michael"] + commits_freq["Rizwan Ahmad Lubis"] + commits_freq["avnandu"] +commits_freq["Rileigh"]);
    yasce.innerHTML = "Commits: "+ commits_freq["Yaswin Veluguleti"];
    michce.innerHTML = "Commits: "+ commits_freq["michael"];
    rizce.innerHTML = "Commits: "+ commits_freq["Rizwan Ahmad Lubis"];
    avce.innerHTML = "Commits: "+ commits_freq["avnandu"];
    rilce.innerHTML = "Commits: "+ commits_freq["Rileigh"];
}


function renderGitlabCommitsWithPagination()
{
    // Used to assert that only us 5 are making commits to the repo
    // Commiting directly from gitlab uses a different name
    var commits_freq = {"Yaswin Veluguleti" : 0, "michael" : 2, "Rizwan Ahmad Lubis" : 0, "Rileigh" : 0, "avnandu" : 1};

	var total_pages = 1;
	// initial used to get value of X-Total-Page
	var initial = $.ajax({
	    type: 'GET',
	    dataType: 'JSON',
	    data: {
	        'per_page' : 100,
	        'page' : 1
	    },
	    url : CommitsUrl,
	    success: function(data) {
	        total_pages = parseInt(initial.getResponseHeader('X-Total-Pages'));
	        countPage(data, commits_freq);
	        if (total_pages === 1)
            	updateCommitHTML(commits_freq);
	    },
	    complete: function(){
	    	for(var i = 2; i <= total_pages; i++)
	    	{
				$.ajax({
				    type: 'GET',
				    dataType: 'JSON',
			        data: {
			            'per_page' : 100,
			            'page' : i
			        },
				    url:CommitsUrl,
				    success: function(data) {
				    	countPage(data, commits_freq);
				    },
				    complete: function(){
				    	if(i === total_pages + 1)
				    		updateCommitHTML(commits_freq);
				    }
				});
	    	}
	    }
	});
}


$.get(IssuesUrl,'per_page=100', renderGitlabIssues);
renderGitlabCommitsWithPagination();



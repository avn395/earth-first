# import the Flask class from the flask module
from flask import Flask, render_template, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# import requests to allow API requests from the public APIs
import requests
import json
from flask import jsonify, request

# other random imports
import time

# create the application object
application = Flask(__name__)

# Enable CORS
CORS(application)

# create the connection to SQL
db_uri = (
    "mysql://Mercury:earthfirst@newdb.crgt7cpj8d6t.us-east-2.rds.amazonaws.com/EADB"
)
application.config["SQLALCHEMY_DATABASE_URI"] = db_uri
application.config["SQLALCHEMY_POOL_RECYCLE"] = 3600
db = SQLAlchemy(application)

# SQLAlchemy model imports
from models import *

db.create_all()


# use decorators to link the function to a url
@application.route("/")
def index():
    return render_template("index.html")


@application.route("/politics_search/<search_term>")
def politics_search(search_term):
    return render_template("politics_search.html", search_term=search_term)


@application.route("/orgs_search/<search_term>")
def orgs_search(search_term):
    return render_template("orgs_search.html", search_term=search_term)


@application.route("/states_search/<search_term>")
def states_search(search_term):
    return render_template("states_search.html", search_term=search_term)


@application.route("/search/<search_term>")
def general_search(search_term):
    return render_template("general_search.html", search_term=search_term)


@application.route("/visualization")
def visualization():
    return render_template("visualization.html")


@application.route("/customer")
def customer_visualization():
    return render_template("customer.html")


@application.route("/orgs")
def orgs():
    return render_template("orgs.html")


@application.route("/orgs/<org_name>")
def org_instance(org_name):
    return render_template("/org_pages/orgs_instance.html", name=org_name)


@application.route("/politics")
def politics():
    return render_template("politics.html")


@application.route("/politics/<bill_id>")
def bill_instance(bill_id):
    return render_template("/bill_pages/bill_instance.html", id=bill_id)


@application.route("/states")
def states():
    return render_template("states.html")


@application.route("/about")
def about():
    return render_template("about.html")


@application.route("/states/<state_name>")
def state_instance(state_name):
    return render_template("/state_pages/state_instance.html", state=state_name)


# -----------
# |API Calls|
# -----------

# I don't think we should use a database for this one cuz it's not linear
# @application.route("/api/about_me/issues", methods=['GET'])
def about_me_issues_get():

    # Get Database information
    devs = About.query.all()
    latestIssue = devs[0].last_issue

    # Get https response for latest set of issues
    gitIssuesURL = "https://gitlab.com/api/v4/projects/10950119/issues"
    gitIssuesParams = {"per_page": "100", "page": "1"}
    r = requests.get(url=gitIssuesURL, params=gitIssuesParams)
    data = r.json()

    # Check if latest issue == latest_issue from db
    if data[0]["id"] == latestIssue:
        # If so, return database info
        return jsonify(constructIssues(devs))
    else:
        # Else, calc until issue matches database info
        shouldExit = False
        newOpened = {
            "Yaswin Veluguleti": 0,
            "michael": 0,
            "Rizwan Ahmad Lubis": 0,
            "Rileigh": 0,
            "avnandu": 0,
        }
        newClosed = {
            "Yaswin Veluguleti": 0,
            "michael": 0,
            "Rizwan Ahmad Lubis": 0,
            "Rileigh": 0,
            "avnandu": 0,
        }
        nextCommitID = data[0]["id"]
        # Update database
        # Return updated info


def constructIssues(devs):
    ans = {}
    ans["issues_opened"] = {}
    ans["issues_closed"] = {}
    for dev in devs:
        ans["issues_opened"][dev.name] = dev.issues_opened
        ans["issues_closed"][dev.name] = dev.issues_closed
    return ans


@application.route("/api/about_me/commits", methods=["GET"])
def about_me_commits_get():

    # Get database information
    devs = About.query.all()
    latestCommit = devs[0].last_commit

    # Get https response for latest set of commits
    gitCommitsURL = "https://gitlab.com/api/v4/projects/10950119/repository/commits"
    gitCommitsParams = {"per_page": "100", "page": "1"}
    r = requests.get(url=gitCommitsURL, params=gitCommitsParams)
    data = r.json()

    # Check if latest commit == latest_commit from db
    if data[0]["id"] == latestCommit:
        # If so, return database info
        return jsonify(constructCommits(devs))
    else:
        # Else, calc until commit matches with database info
        shouldExit = False
        newCommits = {
            "Yaswin Veluguleti": 0,
            "michael": 0,
            "Rizwan Ahmad Lubis": 0,
            "Rileigh": 0,
            "avnandu": 0,
        }
        nextCommitID = data[0]["id"]

        while not shouldExit:
            print("doing loop")
            for commit in data:
                if commit["id"] == latestCommit:
                    shouldExit = True
                    break
                if (
                    commit["author_name"] == "Michael Lee"
                    or commit["author_name"] == "Avni Nandu"
                ):
                    pass
                else:
                    newCommits[commit["author_name"]] += 1
            if not shouldExit:
                gitCommitsParams["page"] = str(int(gitCommitsParams["page"]) + 1)
                r = requests.get(url=gitCommitsURL, params=gitCommitsParams)
                data = r.json()
                if len(data) == 0:
                    shouldExit = True
                    break

        # Update database
        for dev in devs:
            if dev.name == "Yaswin":
                dev.commit_count += newCommits["Yaswin Veluguleti"]
            elif dev.name == "Michael":
                dev.commit_count += newCommits["michael"]
            elif dev.name == "Riz":
                dev.commit_count += newCommits["Rizwan Ahmad Lubis"]
            elif dev.name == "Rileigh":
                dev.commit_count += newCommits["Rileigh"]
            elif dev.name == "Avni":
                dev.commit_count += newCommits["avnandu"]
            dev.last_commit = nextCommitID

        db.session.commit()

        # Return updated info
        return jsonify(constructCommits(devs))


def constructCommits(devs):
    ans = {}
    for dev in devs:
        ans[dev.name] = dev.commit_count
        if dev.name == "Michael":
            ans[dev.name] += 3
        if dev.name == "Avni":
            ans[dev.name] += 1

    return ans


# @application.before_request
# def before_request():
#    print(request.url)
#    if request.url.startswith("https"):
#        return
#
#    if request.url.startswith("http://www.ea") or request.url.startswith("http://ea"):
#        url = request.url.replace("http://", "https://", 1)
#        code = 301
#        return redirect(url, code=code)
#    elif request.url.startswith("ea") or request.url.startswith("www.ea"):
#        url = "https://" + request.url
#        code = 301
#        return redirect(url, code=code)


@application.route("/api/states", methods=["GET"])
def get_state_instances():
    # checks for state parameters (state=TX). If there is a state, returns
    # programs in that state. If no state, returns all programs.
    col_names = {
        "abbr",
        "state",
        "population",
        "image_url",
        "num_bills",
        "num_orgs",
        "num_progs",
    }
    kwargs = {key: value for key, value in request.args.items() if key in col_names}
    args = {key: value for key, value in request.args.items()}

    # per_page = 9
    # page = 1
    # if "per_page" in args:
    #     per_page = int(args["per_page"])
    # if "page" in args:
    #     page = int(args["page"])

    # pagination = State.query.filter_by(**kwargs).paginate(page=page, per_page=per_page)

    # states = (
    #     pagination.items
    # )

    # return jsonify({'data' : [s.serialize for s in states], 'total' : pagination.total})
    if "search" in args:
        val = "%" + args["search"] + "%"
        states = State.query.filter(
            State.abbr.like(val) | State.state.like(val) | State.region.like(val)
        ).all()
    else:
        states = State.query.filter_by(**kwargs)
    return jsonify([s.serialize for s in states])


@application.route("/api/programs", methods=["GET"])
def get_program_instances():
    # checks for state parameters (state=TX). If there is a state, returns
    # programs in that state. If no state, returns all programs.
    col_names = {
        "Id",
        "Name",
        "City",
        "State",
        "Hours",
        "Phone",
        "Description",
        "Url",
        "ProgramType",
    }
    kwargs = {key: value for key, value in request.args.items() if key in col_names}
    args = {key: value for key, value in request.args.items()}

    if "composting" in args:
        if bool(args["composting"]):
            kwargs[
                "ProgramType"
            ] = "Government Organics Curbside - A program operated with city, county or regional government funding that accepts organic materials, such as yard waste or food waste (defaults as first listing in search results)."
    elif "curbside" in args:
        if bool(args["curbside"]):
            kwargs[
                "ProgramType"
            ] = "Government Packaging Curbside - A program operated with city, county or regional government funding that accepts packaging materials, such as glass, metal, paper or plastic (defaults as first listing in search results)."

    per_page = 9
    page = 1
    if "per_page" in args:
        per_page = int(args["per_page"])
    if "page" in args:
        page = int(args["page"])

    programs = (
        Program.query.filter_by(**kwargs).paginate(page=page, per_page=per_page).items
    )
    return jsonify([p.serialize for p in programs])


@application.route("/api/orgs", methods=["GET"])
def get_org_instances():
    col_names = {
        "name",
        "mission",
        "tagline",
        "url",
        "state",
        "city",
        "address",
        "postal_code",
        "rating",
    }
    kwargs = {key: value for key, value in request.args.items() if key in col_names}
    args = {key: value for key, value in request.args.items()}

    # per_page = 9
    # page = 1
    # if "per_page" in args:
    #     per_page = int(args["per_page"])
    # if "page" in args:
    #     page = int(args["page"])

    if "search" in args:
        val = "%" + args["search"] + "%"
        orgs = Org.query.filter(
            Org.name.like(val)
            | Org.mission.like(val)
            | Org.tagline.like(val)
            | Org.state.like(val)
            | Org.city.like(val)
            | Org.address.like(val)
            | Org.postal_code.like(val)
            | Org.rating.like(val)
        ).all()
    else:
        orgs = Org.query.filter_by(**kwargs)
    return jsonify([o.serialize for o in orgs])


@application.route("/api/bills", methods=["GET"])
def get_bill_instances():
    col_names = {
        "bill_id",
        "title",
        "description",
        "sponsor_name",
        "sponsor_state",
        "sponsor_party",
        "sponsor_image",
        "bill_pdf",
        "intro_date",
        "latest_major_action",
    }

    kwargs = {key: value for key, value in request.args.items() if key in col_names}
    args = {key: value for key, value in request.args.items()}

    # per_page = 9
    # page = 1
    # if "per_page" in args:
    #     per_page = int(args["per_page"])
    # if "page" in args:
    #     page = int(args["page"])

    if "search" in args:
        val = "%" + args["search"] + "%"
        bills = Bill.query.filter(
            Bill.title.like(val)
            | Bill.description.like(val)
            | Bill.sponsor_name.like(val)
            | Bill.sponsor_state.like(val)
        ).all()
    else:
        bills = Bill.query.filter_by(**kwargs)
    return jsonify([b.serialize for b in bills])


# @application.route("/api/states/search", methods=["GET"])
# def get_states_search_results():
#     # Requires state and search parameters.
#     state = request.args["state"]
#     val = "%" + request.args["search"] + "%"
#     programs = Program.query.filter_by()


# start the server with the 'run()' method
# if __name__ == "__main__":
#    pass
# context= ('local.crt', 'local.key')
# application.run(ssl_context=context)
# application.run()

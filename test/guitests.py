from selenium import webdriver
from unittest import main, TestCase
import time

chrome_webdriver_addr = "./tests/webdrivers/chromedriver"
firefox_webdriver_addr = "./tests/webdrivers/geckodriver"
safari_webdriver_addr = "./tests/webdrivers/safaridriver"
EF_url = "https://earth-first.live/"


class SeleniumUnitTests(TestCase):
    def test_open_on_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        browser.quit()

    def test_open_on_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        browser.quit()

    def test_open_on_safari(self):
        browser = webdriver.Safari(executable_path=safari_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        browser.quit()

    def test_click_aboutme_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("About Us")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_orgs_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Organizations")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_politics_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Politics")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_state_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("States")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_orgs_inst_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Organizations")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_partial_link_text("American")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_politics_inst_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Politics")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_partial_link_text("Consolidated")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_state_inst_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("States")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_partial_link_text("Texas")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_gitlab_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("About Us")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_link_text("GitLab Repository")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_postman_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("About Us")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_link_text("Postman")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_back_home_chrome(self):
        browser = webdriver.Chrome(chrome_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("States")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_link_text("Home")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_aboutme_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("About Us")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_orgs_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Organizations")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_politics_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Politics")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_state_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("States")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_orgs_inst_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Organizations")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_partial_link_text("American")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_politics_inst_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("Politics")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_partial_link_text("Consolidated")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_state_inst_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("States")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_partial_link_text("Texas")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_gitlab_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("About Us")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_link_text("GitLab Repository")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_postman_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("About Us")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_link_text("Postman")
        link.click()
        time.sleep(3)
        browser.quit()

    def test_click_back_home_firefox(self):
        browser = webdriver.Firefox(executable_path=firefox_webdriver_addr)
        browser.get(EF_url)
        time.sleep(3)
        link = browser.find_element_by_link_text("States")
        link.click()
        time.sleep(3)
        link = browser.find_element_by_link_text("Home")
        link.click()
        time.sleep(3)
        browser.quit()


if __name__ == "__main__":
    main()

from unittest import main, TestCase
import sys

# Add root dir to path
sys.path.append("../")
sys.path.append("./")
from application import *
from models import *
import json


class EarthFirstUnitTests(TestCase):
    def test_bill_model1(self):
        a = Bill()
        self.assertEqual(str(type(a)), "<class 'models.Bill'>")
        data = a.serialize
        self.assertTrue("bill_id" in data)
        self.assertTrue("title" in data)
        self.assertTrue("description" in data)

    def test_bill_model2(self):
        a = Bill()
        self.assertEqual(str(type(a)), "<class 'models.Bill'>")
        data = a.serialize
        self.assertTrue("sponsor_name" in data)
        self.assertTrue("sponsor_state" in data)
        self.assertTrue("sponsor_party" in data)
        self.assertTrue("sponsor_image" in data)

    def test_bill_model3(self):
        a = Bill()
        self.assertEqual(str(type(a)), "<class 'models.Bill'>")
        data = a.serialize
        self.assertTrue("bill_pdf" in data)
        self.assertTrue("intro_date" in data)
        self.assertTrue("latest_major_action" in data)

    def test_Org_model1(self):
        a = Org()
        self.assertEqual(str(type(a)), "<class 'models.Org'>")
        data = a.serialize
        self.assertTrue("name" in data)
        self.assertTrue("mission" in data)
        self.assertTrue("tagline" in data)

    def test_Org_model2(self):
        a = Org()
        self.assertEqual(str(type(a)), "<class 'models.Org'>")
        data = a.serialize
        self.assertTrue("state" in data)
        self.assertTrue("city" in data)
        self.assertTrue("address" in data)
        self.assertTrue("postal_code" in data)

    def test_Org_model3(self):
        a = Org()
        self.assertEqual(str(type(a)), "<class 'models.Org'>")
        data = a.serialize
        self.assertTrue("url" in data)
        self.assertTrue("rating" in data)
        self.assertTrue("image_url" in data)

    def test_Program_model1(self):
        a = Program()
        self.assertEqual(str(type(a)), "<class 'models.Program'>")
        data = a.serialize
        self.assertTrue("Id" in data)
        self.assertTrue("Name" in data)
        self.assertTrue("City" in data)
        self.assertTrue("State" in data)

    def test_Program_model2(self):
        a = Program()
        self.assertEqual(str(type(a)), "<class 'models.Program'>")
        data = a.serialize
        self.assertTrue("Hours" in data)
        self.assertTrue("Phone" in data)

    def test_Program_model3(self):
        a = Program()
        self.assertEqual(str(type(a)), "<class 'models.Program'>")
        data = a.serialize
        self.assertTrue("Description" in data)
        self.assertTrue("Url" in data)
        self.assertTrue("ProgramType" in data)

    def test_About_devs(self):
        self.assertEqual(
            str(About.query.all()), "[Avni, Michael, Rileigh, Riz, Yaswin]"
        )
        self.assertEqual(str(type(About.query.all())), "<class 'list'>")

    def test_Construct_Issues(self):
        a = About.query.all()
        Issues = constructIssues(a)
        self.assertEqual(str(type(Issues)), "<class 'dict'>")
        for dev in a:
            self.assertTrue(repr(dev) in Issues["issues_opened"])
            self.assertTrue(repr(dev) in Issues["issues_closed"])

    def test_Construct_Commits(self):
        a = About.query.all()
        Commits = constructCommits(a)
        self.assertEqual(str(type(Commits)), "<class 'dict'>")
        for dev in a:
            self.assertTrue(repr(dev) in Commits)

    def test_Construct_Issues1(self):
        a = About.query.all()
        Issues = constructIssues(a)
        self.assertEqual(str(type(Issues)), "<class 'dict'>")
        for dev in a:
            self.assertTrue(Issues["issues_opened"][repr(dev)] != None)
            self.assertTrue(Issues["issues_closed"][repr(dev)] != None)

    def test_Construct_Commits1(self):
        a = About.query.all()
        Commits = constructCommits(a)
        self.assertEqual(str(type(Commits)), "<class 'dict'>")
        for dev in a:
            self.assertTrue(Commits[repr(dev)] != None)

    def test_bill_model4(self):
        a = Bill()
        self.assertEqual(str(type(a)), "<class 'models.Bill'>")
        data = a.serialize
        self.assertFalse(data["bill_id"])
        self.assertFalse(data["title"])
        self.assertFalse(data["description"])

    def test_bill_model5(self):
        a = Bill()
        self.assertEqual(str(type(a)), "<class 'models.Bill'>")
        data = a.serialize
        self.assertFalse(data["sponsor_name"])
        self.assertFalse(data["sponsor_state"])
        self.assertFalse(data["sponsor_party"])
        self.assertFalse(data["sponsor_image"])

    def test_bill_model6(self):
        a = Bill()
        self.assertEqual(str(type(a)), "<class 'models.Bill'>")
        data = a.serialize
        self.assertFalse(data["bill_pdf"])
        self.assertFalse(data["intro_date"])
        self.assertFalse(data["latest_major_action"])

    def test_Org_model4(self):
        a = Org()
        self.assertEqual(str(type(a)), "<class 'models.Org'>")
        data = a.serialize
        self.assertFalse(data["name"])
        self.assertFalse(data["mission"])
        self.assertFalse(data["tagline"])

    def test_Org_model5(self):
        a = Org()
        self.assertEqual(str(type(a)), "<class 'models.Org'>")
        data = a.serialize
        self.assertFalse(data["state"])
        self.assertFalse(data["city"])
        self.assertFalse(data["address"])
        self.assertFalse(data["postal_code"])

    def test_Org_model6(self):
        a = Org()
        self.assertEqual(str(type(a)), "<class 'models.Org'>")
        data = a.serialize
        self.assertFalse(data["url"])
        self.assertFalse(data["rating"])
        self.assertFalse(data["image_url"])

    def test_Program_model4(self):
        a = Program()
        self.assertEqual(str(type(a)), "<class 'models.Program'>")
        data = a.serialize
        self.assertFalse(data["Id"])
        self.assertFalse(data["Name"])
        self.assertFalse(data["City"])
        self.assertFalse(data["State"])

    def test_Program_model5(self):
        a = Program()
        self.assertEqual(str(type(a)), "<class 'models.Program'>")
        data = a.serialize
        self.assertFalse(data["Hours"])
        self.assertFalse(data["Phone"])

    def test_Program_model6(self):
        a = Program()
        self.assertEqual(str(type(a)), "<class 'models.Program'>")
        data = a.serialize
        self.assertFalse(data["Description"])
        self.assertFalse(data["Url"])
        self.assertFalse(data["ProgramType"])


if __name__ == "__main__":  # pragma: no cover
    main()

var chai = require('chai');
const expect = require("chai").expect;
const assert = require("chai").assert;
const request = require('request');
const API_URL = "https://earth-first.live/api/"

function perform_api_call(url) {
  return new Promise((resolve, reject) => {
    request({
      "url": url,
      "method": "GET"
    }, (error, response, body) => {
      if (error) {
        reject(new Error('Unable to connect to API'));
      } else {
        try{
          resolve(JSON.parse(body));
        }
        catch(e)
        {
          resolve({})
        }
      }
    });
  });
}


describe('Bill Phase 2', function() {
  describe('Testing bills API', function() {
    it('should return a JSON',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.equal(typeof a, "object")
    });
  });

  describe('Testing bills API', function() {
    it('should be nonempty',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.isAbove(a.length, 0)
    });
  });

  describe('Testing bills API', function() {
    it('should have title, description, and id',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.equal("bill_id" in a[0], true);
      assert.equal("title" in a[0], true);
      assert.equal("description" in a[0], true);
    });
  });


  describe('Testing bills API', function() {
    it('should have sponsor info',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.equal("sponsor_name" in a[0], true);
      assert.equal("sponsor_state" in a[0], true);
      assert.equal("sponsor_party" in a[0], true);
      assert.equal("sponsor_image" in a[0], true);
    });
  });
});




describe('Org Phase 2', function() {
  describe('Testing orgs API', function() {
    it('should return a JSON',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.equal(typeof a, "object")
    });
  });

  describe('Testing orgs API', function() {
    it('should be nonempty',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.isAbove(a.length, 0)
    });
  });

  describe('Testing orgs API', function() {
    it('should have basic org info',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.equal("name" in a[0], true);
      assert.equal("mission" in a[0], true);
      assert.equal("tagline" in a[0], true);
    });
  });


  describe('Testing orgs API', function() {
    it('should have location info',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.equal("state" in a[0], true);
      assert.equal("city" in a[0], true);
      assert.equal("address" in a[0], true);
      assert.equal("postal_code" in a[0], true);
    });
  });
});




describe('States Phase 2', function() {
  describe('Testing states API', function() {
    it('should return a JSON',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.equal(typeof a, "object")
    });
  });

  describe('Testing states API', function() {
    it('should be nonempty',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.isAbove(a.length, 0)
    });
  });

  describe('Testing states API', function() {
    it('should have basic state info',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.equal("state" in a[0], true);
      assert.equal("abbr" in a[0], true);
      assert.equal("population" in a[0], true);
    });
  });


  describe('Testing states API', function() {
    it('should have number of associated models',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.equal("num_bills" in a[0], true);
      assert.equal("num_orgs" in a[0], true);
      assert.equal("num_progs" in a[0], true);
    });
  });
});




describe('Bill Phase 3', function() {
  describe('Testing Bills API', function() {
    it('should return a JSON',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.equal(typeof a, "object")
    });
  });
  describe('Testing bills API', function() {
    it('should be nonempty',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.isAbove(a.length, 0)
    });
  });

  describe('Testing bills API', function() {
    it('should have info on bill action',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.equal("bill_pdf" in a[0], true);
      assert.equal("intro_date" in a[0], true);
      assert.equal("latest_major_action" in a[0], true);
    });
  });


  describe('Testing bills API', function() {
    it('should have all bill info',async function() {
      let a = await perform_api_call(API_URL + "bills");
      assert.equal("bill_id" in a[0], true);
      assert.equal("title" in a[0], true);
      assert.equal("description" in a[0], true);
      assert.equal("sponsor_name" in a[0], true);
      assert.equal("sponsor_state" in a[0], true);
      assert.equal("sponsor_party" in a[0], true);
      assert.equal("sponsor_image" in a[0], true);
      assert.equal("bill_pdf" in a[0], true);
      assert.equal("intro_date" in a[0], true);
      assert.equal("latest_major_action" in a[0], true);
    });
  });
});




describe('Org Phase 3', function() {
  describe('Testing Orgs API', function() {
    it('should return a JSON',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.equal(typeof a, "object")
    });
  });
  describe('Testing Orgs API', function() {
    it('should be nonempty',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.isAbove(a.length, 0)
    });
  });

  describe('Testing orgs API', function() {
    it('should have media info',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.equal("url" in a[0], true);
      assert.equal("rating" in a[0], true);
      assert.equal("image_url" in a[0], true);
    });
  });


  describe('Testing orgs API', function() {
    it('should have all org info',async function() {
      let a = await perform_api_call(API_URL + "orgs");
      assert.equal("name" in a[0], true);
      assert.equal("mission" in a[0], true);
      assert.equal("tagline" in a[0], true);
      assert.equal("state" in a[0], true);
      assert.equal("city" in a[0], true);
      assert.equal("address" in a[0], true);
      assert.equal("postal_code" in a[0], true);
      assert.equal("url" in a[0], true);
      assert.equal("rating" in a[0], true);
      assert.equal("image_url" in a[0], true);
    });
  });
});




describe('States Phase 3', function() {
  describe('Testing states API', function() {
    it('should return a JSON',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.equal(typeof a, "object")
    });
  });

  describe('Testing states API', function() {
    it('should be nonempty',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.isAbove(a.length, 0)
    });
  });

  describe('Testing states API', function() {
    it('should have program contact info',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.equal("state" in a[0], true);
      assert.equal("abbr" in a[0], true);
      assert.equal("population" in a[0], true);
    });
  });


  describe('Testing states API', function() {
    it('should have all state model info',async function() {
      let a = await perform_api_call(API_URL + "states");
      assert.equal("state" in a[0], true);
      assert.equal("abbr" in a[0], true);
      assert.equal("population" in a[0], true);
      assert.equal("num_bills" in a[0], true);
      assert.equal("num_orgs" in a[0], true);
      assert.equal("num_progs" in a[0], true);
    });
  });
});